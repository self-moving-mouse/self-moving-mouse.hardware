import sys

import cadquery as cq
from cadquery.selectors import NearestToPointSelector, AreaNthSelector, BoxSelector, SumSelector
from cadquery import exporters

if "case_dimensions" in sys.modules:
    del sys.modules["case_dimensions"]
if "case_export" in sys.modules:
    del sys.modules["case_export"]
if "cadquery_util" in sys.modules:
    del sys.modules["cadquery_util"]

from case_dimensions import *
from case_export import \
    save_svg,\
    make_compund,\
    export_part_model,\
    import_board
from cadquery_util import *

def load_logo_sketch():
    """
    loads and scales logo sketch
    from DXF contour files
    """    
    return (
        setSketchSize(
            centeredSketch(
                createSketchFromDXFs(    
                    [
                        ("a", "logo/logo-OuterContour.dxf"),
                        ("s", "logo/logo-InnerCircle.dxf"),
                        ("s", "logo/logo-InnerBottomHole.dxf"),
                        ("a", "logo/logo-MouseOuterControur.dxf"),
                        ("s", "logo/logo-MouseInnerContour.dxf"),
                        ("a", "logo/logo-MouseEye.dxf"),
                        ("a", "logo/logo-OuterParentheses.dxf"),
                    ],
                )),
            width = logo_width))

def get_inner_volume_crossection(workplane):
    """
    Creates case inner volume crossection wire
    """
    return \
      workplane\
      .moveTo(
          inner_volume_width/2,0)\
      .vLine(inner_volume_side_height/2)\
      .threePointArc(
          (0,inner_volume_total_height/2),
          (-inner_volume_width/2,inner_volume_side_height/2))\
      .vLine(-inner_volume_side_height/2)\
      .mirrorX()

def get_inner_volume():
    """
    Creates case inner volume
    (not needed by itself but
    will be used as a tool to
    construct other parts)
    """
    return \
        get_inner_volume_crossection(
            cq.Workplane("XZ"))\
        .extrude(inner_volume_back_half_length)\
        .wires(">Y")\
        .toPending()\
        .workplane()\
        .extrude(-inner_volume_front_half_length)

def get_case_shell():
    """
    Creates case outer shell
    without any openings
    """
    return \
        get_inner_volume()\
        .shell(wall_thickness)

def prepare_logo_top_stamp():
    """
    Prepares a tool to engrave (stamp)
    logo onto top case half outer surface.
    """
    case_full_volume = \
        get_inner_volume()\
        .union(
            get_inner_volume()
            .shell(wall_thickness-logo_depth))

    extruded_logo = \
        cq.Workplane("XY")\
        .placeSketch(
            load_logo_sketch()
            .moved(cq.Location(cq.Vector(
                0, logo_center_offset, 0))))\
        .extrude(inner_volume_total_height)

    return \
        extruded_logo \
        .cut(case_full_volume)


def prepare_logo_bottom_stamp():
    """
    Prepares a tool to engrave (stamp)
    logo onto bottom case half outer surface.
    """
    case_full_volume = \
        get_inner_volume()\
        .union(
            get_inner_volume()
            .shell(wall_thickness-logo_depth))

    extruded_logo = \
        cq.Workplane("XY")\
        .placeSketch(
            load_logo_sketch()
            .moved(cq.Location(cq.Vector(
                0, logo_center_offset, 0))))\
        .extrude(-inner_volume_total_height)

    return \
        extruded_logo \
        .cut(case_full_volume)

def thicken_front_wall(case_shell):
    """
    Thickens front wall.
    Front wall needs to be thicker
    to provide side support for USB connector
    and to reduce the connector
    stickout to 12 mm
    """
    return \
    case_shell\
    .faces(">Y[-2]")\
    .wires()\
    .toPending()\
    .workplane()\
    .extrude(front_wall_thickening)

def thicken_back_wall(case_shell):
    """
    Thickens back wall.
    Back wall needs to be thicker
    to accommodate button pusher plate
    """
    return \
    case_shell\
    .faces("<Y[-2]")\
    .wires()\
    .toPending()\
    .workplane()\
    .extrude(back_wall_thickening)

def get_non_split_case():
    return \
        thicken_back_wall(
            thicken_front_wall(
              get_case_shell()))

def get_case_top_no_lip(case):
    """
    Top part of case shell
    """
    # Overlaps with bottom part on purpose
    # this is required to make lip later
    return \
        splitXYTop(
            case,
            case_flange_top_z)

def get_case_bottom_no_lip(case):
    """
    Bottom half if case shell
    """
    # Overlaps with top part on purpose
    # this is required to make lip later
    return \
        splitXYBottom(
            case,
            case_flange_top_z -
                board_compression_flange_clearance)

def cut_bottom_lip(case_bottom):
    """
    Cuts lip on bottom case half rim
    """
    return case_bottom\
    .faces(">Z")\
    .wires(AreaNthSelector(-1))\
    .toPending()\
    .offset2D(
        -(wall_thickness/2-flange_clearance/2))\
    .cutBlind(-flange_height)


def cut_top_lip(case_top):
    """
    Cuts lip on top case half rim
    """
    return case_top\
    .faces("<Z")\
    .wires(AreaNthSelector(-1))\
    .toPending()\
    .offset2D(
        -(wall_thickness/2+flange_clearance/2))\
    .extrude(-flange_height)\
    .faces("<Z[-2]")\
    .wires(AreaNthSelector(-3))\
    .toPending()\
    .cutBlind(-flange_height*2)

def cut_usb_window(case_half):
    """
    Cuts a window for USB connector
    in top or bottom case half
    """
    return case_half\
        .cut(
            cq.Workplane("XZ")\
            .rect(
                usb_window_width,
                usb_window_height)\
            .extrude(-inner_volume_front_half_length*1.5))

def cut_button_pusher_well(case_half):
    """
    Cuts button pusher well
    in top or bottom case half
    """
    return case_half\
        .cut(
            cq.Workplane("XZ")\
            .circle(button_pusher_plate_hole_dia/2.)\
            .extrude(inner_volume_back_half_length)\
            .faces("<Y")\
            .workplane()\
            .circle(button_pusher_shank_hole_dia/2.)\
            .extrude(wall_thickness*2))

def cut_button_pusher_well_overhang(case_top):
    """
    removes overhanging parts
    of button pusher plate well
    on case top half so that
    support material is not required
    when 3D printing and button
    pusher can be easily inserted
    from above
    """
    return case_top\
        .cut(
            cq.Workplane("XZ")
            .move(0,-flange_height)\
            .rect(button_pusher_plate_hole_dia, flange_height*2)\
            .extrude(inner_volume_back_half_length)\
            .rect(button_pusher_shank_hole_dia, flange_height*2)\
            .extrude(inner_volume_back_half_length+wall_thickness*2))

def get_case_bottom_no_screwhole():
    return cut_button_pusher_well(
        cut_usb_window(
            cut_bottom_lip(
                get_case_bottom_no_lip(
                    get_non_split_case()))))


def get_case_top_no_screwhole():
    return cut_button_pusher_well_overhang(
        cut_button_pusher_well(
            cut_usb_window(
                cut_top_lip(
                    get_case_top_no_lip(
                        get_non_split_case())))))

def get_screwhole_bottom_add():
    """
    Thickens case bottom to
    support the board. The support
    leaves space near back wall to
    make room for button pusher
    and near front wall to accommodate
    legs of throughole USB A connector.
    """
    return\
    cq.Workplane("XY")\
    .workplane(-board_bottom_z_offset)\
    .move(
        -inner_volume_width/2,
        -screw_head_well_outer_dia/2)\
    .rect(
        inner_volume_width,
        screw_head_well_outer_dia/2 \
        + board_front_half_length \
        - bottom_front_board_support_offset_from_board_edge,
        centered = False)\
    .extrude(-inner_volume_total_height)\
    .intersect(
        get_inner_volume()
        .shell(wall_thickness/2)\
        .union(get_inner_volume()))

def get_screwhole_bottom_cutter():
    """
    Cuts chicago nut head recess in
    bottom half of the case and
    cuts hole for post of the nut
    """
    return\
    cq.Workplane("XY")\
    .transformed(
        offset = cq.Vector(
            0,
            0,
            -board_bottom_z_offset\
            -screw_head_well_wall_thickness))\
    .circle(screw_head_well_inner_dia/2)\
    .extrude(-inner_volume_total_height)\
    .faces(">Z")\
    .workplane()\
    .circle(board_hole_dia/2)\
    .extrude(inner_volume_total_height)

def get_screwhole_top_add():
    """
    Thickens top wall and adds
    material for chicago screw sleeve
    that prevents static discharge spark
    from going from the nut to some
    trace on the board and also pushes the board
    to bottom half of the case when the screw
    is tightened
    """
    return\
    cq.Workplane("XY")\
    .workplane(-board_top_z_offset+top_board_clearance)\
    .circle(top_screw_thread_sleeve_outer_dia/2)\
    .extrude(inner_volume_total_height)\
    .union(
        cq.Workplane("XY")\
        .workplane(board_bottom_z_offset)\
        .move(
            -inner_volume_width/2,
            -screw_head_well_outer_dia/2)\
        .rect(
            inner_volume_width,
            screw_head_well_outer_dia/2 \
            + inner_volume_front_half_length,
            centered = False)\
        .extrude(inner_volume_total_height))\
    .intersect(
        get_inner_volume()
        .shell(wall_thickness/2)\
        .union(get_inner_volume()))

def get_screwhole_top_cutter():
    """
    Cuts chicago screw head recess,
    screw shank hole and hole in
    screw sleeve so that sleeve of
    the bottom half can slide
    inside it
    """
    return\
    cq.Workplane("XY")\
    .workplane(-board_top_z_offset)\
    .circle(board_hole_dia/2)\
    .extrude(
        bottom_screw_post_sleeve_elevation_over_board\
        + screw_post_sleeve_top_clearance)\
    .union(
        cq.Workplane("XY")\
        .workplane(-board_top_z_offset)\
        .circle(top_screw_thread_sleeve_inner_dia/2)\
        .extrude(inner_volume_total_height*2))\
    .union(
        cq.Workplane("XY")\
        .workplane(\
           board_bottom_z_offset\
           +screw_head_well_wall_thickness)\
        .circle(screw_head_well_inner_dia/2)\
        .extrude(inner_volume_total_height*1.5))\
    .union(
        cq.Workplane("XY")\
        .workplane(board_bottom_z_offset)\
        .move(
            -usb_window_width/2,
            inner_front_wall_y_after_thickening)\
        .rect(
            usb_window_width,
            usb_window_width,
            centered = False)\
        .extrude(-usb_window_height/2))

def get_front_bottom_board_support():
    """
    Adds thin ribbons of board support
    material near the front wall around
    USB A connector. The board
    will be pushed against this support
    by similarly shaped parts of case top half
    as the screw is tightened.
    """
    return\
    cq.Workplane("XY")\
    .workplane(-board_bottom_z_offset)\
    .moveTo(
        -inner_volume_width/2,
        board_front_half_length \
        - bottom_front_board_support_offset_from_board_edge)\
    .rect(
        front_vertical_board_support_width,
        board_front_half_length,
        centered = False)\
    .moveTo(
        inner_volume_width/2,
        board_front_half_length \
        - bottom_front_board_support_offset_from_board_edge)\
    .rect(
        -front_vertical_board_support_width,
        board_front_half_length,
        centered = False)\
    .extrude(-inner_volume_total_height)\
    .intersect(
        get_inner_volume()
        .shell(wall_thickness/2)\
        .union(get_inner_volume()))

def get_front_top_board_support():
    """
    Adds thin ribbons of board support
    material near the front wall around
    USB A connector.
    """
    return\
    cq.Workplane("XY")\
    .workplane(
        -board_top_z_offset\
        + top_board_clearance)\
    .moveTo(
        -inner_volume_width/2,
        board_front_half_length \
        - bottom_front_board_support_offset_from_board_edge)\
    .rect(
        front_vertical_board_support_width,
        board_front_half_length,
        centered = False)\
    .moveTo(
        inner_volume_width/2,
        board_front_half_length \
        - bottom_front_board_support_offset_from_board_edge)\
    .rect(
        -front_vertical_board_support_width,
        board_front_half_length,
        centered = False)\
    .extrude(inner_volume_total_height)\
    .intersect(
        get_inner_volume()
        .shell(wall_thickness/4)\
        .union(get_inner_volume()))

def get_board_locator():
    return cq.Workplane("XY")\
        .workplane(-board_bottom_z_offset-0.1)\
        .move(
            - board_width/2,
            board_locator_center_y \
                + board_locator_diameter/2 \
                - locator_clearance)\
        .sagittaArc(
            (- board_width/2,
            board_locator_center_y \
                - board_locator_diameter/2\
                + locator_clearance),
            board_locator_diameter/2-locator_clearance)\
        .line(-wall_thickness, 0)\
        .line(0, board_locator_diameter)\
        .close()\
        .mirrorY()\
        .extrude(
            case_flange_top_z \
            - flange_height \
            + board_bottom_z_offset \
            - flange_clearance+0.1)

def get_case_locator(delta_r):
    return cq.Workplane("YZ")\
        .move(
            0,
            case_flange_top_z - flange_height-flange_clearance)\
        .circle(flange_height+delta_r)\
        .extrude(
            (inner_volume_width + 
              wall_thickness+flange_clearance*2)*0.5, 
            both=True)\
        .cut(get_inner_volume())
        
def get_case_top_no_logo():
    return get_case_top_no_screwhole()\
    .union(get_screwhole_top_add())\
    .cut(get_screwhole_top_cutter())\
    .union(get_front_top_board_support())\
    .cut(get_case_locator(locator_clearance))

def get_case_top():
    return get_case_top_no_logo()\
    .cut(prepare_logo_top_stamp())

def get_case_bottom_no_logo():
    return get_case_bottom_no_screwhole()\
    .union(get_screwhole_bottom_add())\
    .cut(get_screwhole_bottom_cutter())\
    .union(get_front_bottom_board_support())\
    .union(get_board_locator())\
    .union(get_case_locator(0.0))

def get_case_bottom():
    return get_case_bottom_no_logo()\
    .cut(prepare_logo_bottom_stamp())

def get_screw(wp):
    return \
        wp.circle(screw_head_dia/2)\
        .extrude(screw_head_thickness)\
        .faces(">Z")\
        .chamfer(0.5)\
        .faces(">Z")\
        .workplane()\
        .rect(screw_head_dia*1.5,screw_head_slot_width)\
        .cutBlind(-screw_slot_depth)\
        .faces("<Z")\
        .workplane()\
        .circle(screw_thread_dia/2)\
        .extrude(screw_post_length)

def get_nut(wp):
    return \
        wp.circle(screw_head_dia/2)\
        .extrude(-screw_head_thickness)\
        .faces("<Z")\
        .chamfer(0.5)\
        .faces("<Z")\
        .workplane()\
        .rect(screw_head_dia*1.5,screw_head_slot_width)\
        .cutBlind(-screw_slot_depth)\
        .faces(">Z")\
        .workplane()\
        .circle(screw_post_dia/2)\
        .extrude(screw_post_length)\
        .faces(">Z")\
        .workplane()\
        .circle(screw_thread_dia/2*1.1)\
        .cutBlind(-screw_post_length)

def get_screw_positioned():
    return get_screw(
        cq.Workplane(
            "XY",
            origin=(0,0,
                    board_bottom_z_offset\
                    +screw_head_well_wall_thickness)))

def get_nut_positioned():
    return get_nut(
        cq.Workplane(
            "XY",
            origin=(0,0,
                    -board_bottom_z_offset\
                    -screw_head_well_wall_thickness)))

def get_button_pusher(wp):
    return\
        wp\
        .circle(button_pusher_plate_dia/2)\
        .extrude(button_pusher_plate_height)\
        .faces("<Y")\
        .workplane()\
        .circle(button_pusher_shank_dia/2)\
        .extrude(button_pusher_shank_length)\
        .faces("<Y")\
        .edges()\
        .fillet(button_pusher_shank_dia/8)

def get_button_pusher_positioned():
    return get_button_pusher(
        cq.Workplane("XZ")\
          .workplane(-button_pusher_plate_y))

def get_board_positioned():
    return import_board(-board_bottom_z_offset)

def get_device_full():
    return \
        get_case_bottom()\
        .add(get_case_top())\
        .add(get_screw_positioned())\
        .add(get_nut_positioned())\
        .add(get_board_positioned())\
        .add(get_button_pusher_positioned())

def get_device_full_exploded():
    return \
        get_case_bottom()\
        .translate((0,0,-inner_volume_total_height*4))\
        .add(get_case_top()\
            .translate((0,0,inner_volume_total_height*4)))\
        .add(get_screw_positioned()\
            .translate((0,0,inner_volume_total_height*8)))\
        .add(get_nut_positioned()\
            .translate((0,0,-inner_volume_total_height*8)))\
        .add(get_board_positioned())\
        .add(get_button_pusher_positioned())

def cut_case_YZ():
    return \
        splitYZBottom(get_case_bottom())\
        .add(
            splitYZBottom(get_case_top()))\
        .add(
            splitYZBottom(
                get_screw_positioned()))\
        .add(
            splitYZBottom(
                get_nut_positioned()))\
        .add(
            splitYZBottom(
                get_board_positioned()))\
        .add(
            splitYZBottom(
                get_button_pusher_positioned()))

def _cut_zx(offset):
    return \
        splitZXBottom(
            get_case_bottom(),
            offset)\
        .add(
            splitZXBottom(
                get_case_top(),
                offset))\
        .add(
            splitZXBottom(
                get_screw_positioned(),
                offset))\
        .add(
            splitZXBottom(
                get_nut_positioned(),
                offset))\
        .add(
            splitZXBottom(
                get_board_positioned(),
                offset))\
        .add(
            splitZXBottom(
                get_button_pusher_positioned(),
                offset))

def cut_case_ZX_screwhole():
    return _cut_zx(0)

def cut_case_ZX_front_halflength():
    return _cut_zx(inner_volume_front_half_length/2)

def cut_case_ZX_front_support():
    offset = board_front_half_length \
        - bottom_front_board_support_offset_from_board_edge*0.5
    return _cut_zx(offset)

def draw_build_order_illustrations():
    save_svg(
        get_inner_volume_crossection(
            cq.Workplane("XZ")),
        'model_build_sequence/01_inner_volume_crossection.svg',
        height=500)

    save_svg(
        get_inner_volume(),
        'model_build_sequence/02_inner_volume.svg',
        height=400)

    save_svg(
        get_case_shell(),
        'model_build_sequence/03_case_shell.svg',
        height=400)

    save_svg(
        thicken_front_wall(
            get_case_shell()),
        'model_build_sequence/04_front_wall_thickening.svg',
        height=400)

    save_svg(
        thicken_back_wall(
            thicken_front_wall(
                get_case_shell())),
        'model_build_sequence/05_back_wall_thickening.svg',
        height=400)

    save_svg(
        get_case_bottom_no_lip(
            get_non_split_case()),
        'model_build_sequence/06_split_bottom_half.svg',
        height=400)

    save_svg(
        cut_bottom_lip(
            get_case_bottom_no_lip(
                get_non_split_case())),
        'model_build_sequence/07_bottom_lip.svg',
        height=400)

    save_svg(
        cut_usb_window(
            cut_bottom_lip(
                get_case_bottom_no_lip(
                    get_non_split_case()))),
        'model_build_sequence/08_bottom_usb_window.svg',
        height=400)

    save_svg(
        get_case_bottom_no_screwhole(),
        'model_build_sequence/09_bottom_button_pusher_well.svg',
        height=400)

    save_svg(
        get_case_bottom_no_screwhole()\
        .union(get_screwhole_bottom_add())\
        .cut(get_screwhole_bottom_cutter()),
        'model_build_sequence/10_bottom_screwhole.svg',
        height=400)

    save_svg(
        get_case_bottom_no_screwhole()\
        .union(get_screwhole_bottom_add())\
        .cut(get_screwhole_bottom_cutter())\
        .union(get_front_bottom_board_support()),
        'model_build_sequence/11_bottom_front_board_support.svg',
        height=400)

    save_svg(
        get_case_bottom_no_screwhole()\
        .union(get_screwhole_bottom_add())\
        .cut(get_screwhole_bottom_cutter())\
        .union(get_front_bottom_board_support())\
        .union(get_board_locator()),
        'model_build_sequence/12_bottom_board_locator.svg',
        height=400)

    save_svg(
        get_case_bottom_no_logo(),
        'model_build_sequence/13_bottom_case_halves_locator.svg',
        height=400)

    save_svg(
        get_case_bottom(),
        'model_build_sequence/14_bottom_logo.svg',
        height=400)

    save_svg(
        get_case_top_no_lip(
            get_non_split_case()),
        'model_build_sequence/15_split_top_half.svg',
        height=400)

    save_svg(
        cut_top_lip(
            get_case_top_no_lip(
                get_non_split_case())),
        'model_build_sequence/16_top_lip.svg',
        height=400)

    save_svg(
        cut_usb_window(
            cut_top_lip(
                get_case_top_no_lip(
                    get_non_split_case()))),
        'model_build_sequence/17_top_usb_window.svg',
        height=400)

    save_svg(
        get_case_top_no_screwhole(),
        'model_build_sequence/18_top_button_pusher_well.svg',
        height=400)

    save_svg(
        get_case_top_no_screwhole()\
        .union(get_screwhole_top_add())\
        .cut(get_screwhole_top_cutter()),
        'model_build_sequence/19_top_screwhole.svg',
        height=400)

    save_svg(
        get_case_top_no_screwhole()\
        .union(get_screwhole_top_add())\
        .cut(get_screwhole_top_cutter())\
        .union(get_front_top_board_support()),
        'model_build_sequence/20_top_front_board_support.svg',
        height=400)

    save_svg(
        get_case_top_no_logo(),
        'model_build_sequence/21_top_case_halves_locator.svg',
        height=400)


    save_svg(
        get_case_top(),
        'model_build_sequence/22_top_logo.svg',
        height=400)

    save_svg(
        get_button_pusher_positioned(),
        'model_build_sequence/23_button_pusher.svg',
        height=300)

    save_svg(
        get_screw_positioned(),
        'model_build_sequence/24_screw.svg',
        height=500)

    save_svg(
        get_nut_positioned(),
        'model_build_sequence/25_nut.svg',
        height=500)

    save_svg(
        get_board_positioned(),
        'model_build_sequence/26_board.svg',
        height=300)

    save_svg(
        get_case_bottom()\
        .add(get_nut_positioned())\
        .add(get_board_positioned())\
        .add(get_button_pusher_positioned()),
        'model_build_sequence/27_bottom_nut_board_pusher.svg',
        height=300)    

    save_svg(
        get_device_full(),
        'model_build_sequence/28_full_model_with_board.svg',
        height=300)

    save_svg(
        cut_case_YZ(),
        'model_build_sequence/29_case_ZY_cut.svg',
        height=300)

    save_svg(
        cut_case_ZX_screwhole(),
        'model_build_sequence/30_case_ZX_cut_screwhole.svg',
        height=400)

    save_svg(
        cut_case_ZX_front_halflength(),
        'model_build_sequence/31_case_ZX_cut_front_halflength.svg',
        height=400)

    save_svg(
        cut_case_ZX_front_support(),
        'model_build_sequence/32_case_ZX_cut_front_support.svg',
        height=400)
    
    save_svg(
        get_device_full_exploded(),
        'model_build_sequence/33_exploded_device.svg',
        height=300)

def export_model():
    export_part_model(
        get_case_top_no_logo(),
        'models/case-top-no-logo')

    export_part_model(
        get_case_bottom_no_logo(),
        'models/case-bottom-no-logo')

    export_part_model(
        get_case_top(),
        'models/case-top-with-logo')

    export_part_model(
        get_case_bottom(),
        'models/case-bottom-with-logo')

    export_part_model(
        get_board_positioned(),
        'models/board-z-adjusted')

    export_part_model(
        get_button_pusher_positioned(),
        'models/button-pusher')

    export_part_model(
        get_device_full(),
        'models/self-moving-mouse')

    export_part_model(
        cut_case_YZ(),
        'models/self-moving-mouse-yz-cut')

    export_part_model(
        cut_case_ZX_screwhole(),
        'models/self-moving-mouse-zx-cut-back')

draw_build_order_illustrations()

export_model()


bottom = get_case_bottom()
top = get_case_top()
