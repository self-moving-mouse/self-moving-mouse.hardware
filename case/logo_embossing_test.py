import os
import cadquery as cq

box1 = cq.Workplane("XY" ).transformed(offset=(25,25,1)).box(50, 50, 5, centered=(True, True, False))

def importDXFLowTolerance(fname: str):
    return cq.importers.importDXF(
        os.path.abspath(fname), 
        tol=1e-3)

logo_outer_contour = importDXFLowTolerance('logo-OuterContour.dxf')
logo_inner_circle = importDXFLowTolerance('logo-InnerCircle.dxf')
logo_inner_bottom_circle = importDXFLowTolerance('logo-InnerBottomHole.dxf')
logo_mouse_outer_contour = importDXFLowTolerance('logo-MouseOuterControur.dxf')
logo_mouse_inner_contour = importDXFLowTolerance('logo-MouseInnerContour.dxf')
logo_mouse_eye = importDXFLowTolerance('logo-MouseEye.dxf')
logo_outer_parentheses = importDXFLowTolerance('logo-OuterParentheses.dxf')

def extrudeLogoPart(obj):
    return obj.wires().toPending().extrude(3)

mouse = (
    extrudeLogoPart(logo_mouse_outer_contour)
    .cut(extrudeLogoPart(logo_mouse_inner_contour))
    .union(extrudeLogoPart(logo_mouse_eye)))

logo = (
    extrudeLogoPart(logo_outer_contour)
    .cut(extrudeLogoPart(logo_inner_circle))
    .cut(extrudeLogoPart(logo_inner_bottom_circle))
    .union(mouse)
    .union(extrudeLogoPart(logo_outer_parentheses)))

result = box1.cut(logo)
#result

#show_object(
#    result
#    .faces("<Z").wires().fillet(0.5)
#    .faces(">Z[1]").wires().fillet(0.7))



#show_object(logo_inner_circle)