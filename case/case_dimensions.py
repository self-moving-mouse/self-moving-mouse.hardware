# x=0, y=0 is at the center of board hole
# case centerline is at usb connector centerline

# dimensions marked with ?? comment are unknown
# and not important for case fit.
# such values were chosen "by eye" 

board_thickness                  = 1.6
board_width                      = 15.0
board_length                     = 35.0
board_hole_bottom_offset         = 9.45
board_hole_dia                   = 4.3
board_top_hole_courtyard_dia     = 8.0+0.3
board_top_to_usb_top             = 4.5-1.35
board_top_to_usb_halfheight      = 4.5/2-1.35
usb_overhang                     = 15.26
usb_width                        = 11.92
usb_height                       = 4.5
button_overhang                  = 0.55
board_front_edge_to_component    = 6.25
board_locator_center_top_offset  = 5.5
board_locator_diameter           = 1.6

screw_thread_dia            = 3.0
screw_post_dia              = 4.0
# other available screw post lengths 4, 5, 6, 7, 8, 10
screw_post_length           = 6.0  
screw_head_dia              = 10.0
screw_head_thickness        = 1.5  # ??
screw_head_slot_width       = 1    # ??
screw_slot_depth            = 0.7  # ??

wall_thickness                   = 2.2
board_side_clearance             = 0.2
inner_volume_total_height        = 7.5
inner_volume_side_height         = 5.2

usb_stickout                     = 12.0
usb_clearance                    = 0.15

button_pusher_plate_dia          = inner_volume_total_height*0.9
button_pusher_shank_dia          = inner_volume_total_height*0.9-2
button_pusher_clearance          = 0.2
button_pusher_shank_stickout     = 4
button_pusher_plate_height       = 3
button_pusher_plate_to_btn_shank = 0.2

top_board_clearance                            = 0.0
board_compression_flange_clearance             = 0.2
bottom_screw_post_sleeve_elevation_over_board  = 1
screwhole_bottom_ext1                          = 1.5
screw_thread_clearance                         = 0.2
screw_head_clearance                           = 0.2
screw_head_well_wall_thickness                 = 2
screw_post_clearance                           = 0.2

flange_clearance = 0.2
flange_height    = 1

bottom_front_horizontal_board_support_width       = 3.0
bottom_front_board_support_offset_from_board_edge = 6.0
front_vertical_board_support_width                = 0.7+0.3+0.2

case_locator_width = 3.0
# max resolution of typical SLA printer
locator_clearance = 0.05


board_top_to_usb_top = \
    board_top_to_usb_halfheight\
    + usb_height/2

board_back_half_length = board_hole_bottom_offset

board_top_z_offset = board_top_to_usb_halfheight

board_bottom_z_offset = \
    board_top_to_usb_halfheight+board_thickness

inner_volume_width = \
    board_width + board_side_clearance * 2

board_back_half_length = \
    board_hole_bottom_offset

board_front_half_length = \
    board_length \
    - board_hole_bottom_offset

board_locator_center_y = \
    board_front_half_length \
    - board_locator_center_top_offset

front_wall_thickening = \
    + usb_overhang \
    - usb_stickout \
    - wall_thickness \
    - board_side_clearance

inner_volume_front_half_length = \
    board_front_half_length \
    + front_wall_thickening \
    + board_side_clearance

inner_front_wall_y_after_thickening = \
    board_front_half_length \
    + board_side_clearance

# button pusher plate fits entirely inside case
inner_volume_back_half_length = \
    board_back_half_length \
    + max(
        button_overhang + button_pusher_plate_to_btn_shank,
        board_side_clearance) \
    + button_pusher_plate_height

back_wall_thickening = \
    inner_volume_back_half_length \
    - board_back_half_length \
    - board_side_clearance

inner_volume_length = \
    inner_volume_front_half_length \
    + inner_volume_back_half_length

usb_window_width  = usb_width + usb_clearance*2
usb_window_height = usb_height + usb_clearance*2

inner_back_wall_y_after_thickening = \
    - inner_volume_back_half_length \
    + back_wall_thickening

button_pusher_shank_hole_dia = \
    button_pusher_shank_dia + button_pusher_clearance*2

button_pusher_plate_hole_dia = \
    button_pusher_plate_dia + button_pusher_clearance*2

screw_head_well_inner_dia = \
    screw_head_dia + screw_head_clearance*2

screw_head_well_outer_dia = \
    screw_head_well_inner_dia\
    + screw_head_well_wall_thickness*2

top_screw_thread_sleeve_outer_dia = \
    board_top_hole_courtyard_dia

top_screw_thread_sleeve_inner_dia = \
    screw_thread_dia + screw_thread_clearance*2

case_flange_bottom_z = \
    -board_top_z_offset\
    + top_board_clearance

case_top_bottom_separation_z = \
    case_flange_bottom_z \
    + flange_height/2

case_flange_top_z = \
    case_flange_bottom_z + flange_height

button_pusher_shank_length = \
    button_pusher_shank_stickout + \
    wall_thickness

button_pusher_plate_y = \
    - inner_volume_back_half_length \
    + button_pusher_plate_height

screw_post_sleeve_top_clearance = \
    -board_bottom_z_offset \
    -screw_head_well_wall_thickness \
    +screw_post_length \
    +board_compression_flange_clearance*2

logo_width = inner_volume_width
# max engraving depth for JLCPCB 3D printing services
# is currently (2022-02-23) 0.5 mm
logo_depth = 0.4
logo_center_offset = inner_volume_front_half_length * 0.6    