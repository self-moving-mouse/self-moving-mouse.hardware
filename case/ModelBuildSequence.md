# Self-Moving Mouse Case Model

[[_TOC_]]

## Inner volume crossection

![Inner volume crossection](model_build_sequence/01_inner_volume_crossection.svg)

## Inner volume

![Inner volume](model_build_sequence/02_inner_volume.svg)

## Case shell

![Case shell](model_build_sequence/03_case_shell.svg)

## Front wall thickening

![Front wall thickening](model_build_sequence/04_front_wall_thickening.svg)

## Back wall thickening

![Back wall thickening](model_build_sequence/05_back_wall_thickening.svg)

## Splitting bottom half from shell

![Splitting bottom halve](model_build_sequence/06_split_bottom_half.svg)

## Bottom half lip

![Bottom half lip](model_build_sequence/07_bottom_lip.svg)

## Bottom half USB window

![Bottom half USB window](model_build_sequence/08_bottom_usb_window.svg)

## Bottom half button pusher well

![Bottom half button pusher well](model_build_sequence/09_bottom_button_pusher_well.svg)

## Bottom half screw hole

![Bottom half screw hole](model_build_sequence/10_bottom_screwhole.svg)

## Bottom half front board support

![Bottom half front board support](model_build_sequence/11_bottom_front_board_support.svg)

## Bottom half board locator protrusion

![Bottom half board locator protrusion](model_build_sequence/12_bottom_board_locator.svg)

## Bottom half case halves locator

![Bottom half case halves locator](model_build_sequence/13_bottom_case_halves_locator.svg)

## Bottom case half logo engraving

![Bottom case half logo engraving](model_build_sequence/14_bottom_logo.svg)

## Splitting top case half from shell

![Splitting top case half from shell](model_build_sequence/15_split_top_half.svg)

## Top half lip

![Bottom case halves locator](model_build_sequence/16_top_lip.svg)

## Top half USB window

![Top half USB window](model_build_sequence/17_top_usb_window.svg)

## Top half button pusher well

![Top half button pusher well](model_build_sequence/18_top_button_pusher_well.svg)

## Top half screw hole

![Top half screw hole](model_build_sequence/19_top_screwhole.svg)

## Top half front board support

![Top half front board support](model_build_sequence/20_top_front_board_support.svg)

## Top case half case halves locator

![Top case halves locator](model_build_sequence/21_top_case_halves_locator.svg)

## Top case half logo engraving

![Top case halves locator](model_build_sequence/22_top_logo.svg)

## Button pusher

![Button pusher](model_build_sequence/23_button_pusher.svg)

## Screw

![Screw](model_build_sequence/24_screw.svg)

## Nut

![Nut](model_build_sequence/25_nut.svg)

## Board

![Board](model_build_sequence/26_board.svg)

## Bottom half with board and button pusher installed

![Assembled device](model_build_sequence/27_bottom_nut_board_pusher.svg)

## Assembled device

![Assembled device](model_build_sequence/28_full_model_with_board.svg)

## Assembled device cut with ZY plane

![Assembled device cut with ZY plane](model_build_sequence/29_case_ZY_cut.svg)

## Assembled device cut with ZX plane through the screw

![Assembled device cut with ZX plane](model_build_sequence/30_case_ZX_cut_screwhole.svg)

## Assembled device cut with ZX plane front half length

![Assembled device cut with ZX plane front half length](model_build_sequence/31_case_ZX_cut_front_halflength.svg)

## Assembled device cut with ZX plane through front board support

![Assembled device cut with ZX plane through front board support](model_build_sequence/32_case_ZX_cut_front_support.svg)


## Assembled device cut with ZX plane through front board support

![Exploded device model](model_build_sequence/33_exploded_device.svg)