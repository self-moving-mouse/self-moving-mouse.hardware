import itertools
import cadquery as cq
from cadquery import exporters

def make_compund(workplanes):
    vals = list(itertools.chain(*[o.vals() for obj in workplanes for o in obj.all()]))
    return cq.Compound.makeCompound(vals)

def import_board(zoffset):
    return\
    cq.importers.importStep(
        'models/self-moving-mouse-board-no-pinheader.step')\
        .translate(
            (0, 0,  zoffset))

def export_part_model(obj, file_base_path):
     exporters.export(
         obj, 
         file_base_path+'.step')
    
     exporters.export(
         obj, 
         file_base_path+'.stl',
         opt={
             "tolerance": 0.0001,
             "angularTolerance": 0.01
         })

def save_svg(obj, file_path, height=600):    
    exporters.export(
         obj,
         file_path,
         opt={
             "width": 600,
             "height": height,
             "marginLeft": 10,
             "marginTop": 10,
             "showAxes": True,
             "projectionDir": (0.3, 0.5, 0.5),
             "strokeWidth": 0.1,
             "showHidden": True,
         },
     )