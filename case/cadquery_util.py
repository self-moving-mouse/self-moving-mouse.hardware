import sys
import os
from typing import Iterable, Optional

import cadquery as cq
from cadquery.selectors import NearestToPointSelector, AreaNthSelector
from cadquery import BoundBox, exporters

def splitWithPlane(wp, plane, keepTop=False, keepBottom=False):
    old_plane = wp.plane
    wp_new = cq.Workplane(plane)
    wp_new.parent = wp
    wp_new.ctx = wp.ctx
    
    return wp_new.split(
        keepTop=keepTop, 
        keepBottom=keepBottom)

def splitXYTop(wp, z_split=0.0):
    return splitWithPlane(
        wp,
        cq.Plane.named(
            "XY", 
            origin=(0,0,z_split)),
        keepTop = True)
    
def splitXYBottom(wp, z_split=0.0):
    return splitWithPlane(
        wp,
        cq.Plane.named(
            "XY", 
            origin=(0,0,z_split)),
        keepBottom = True)
    
def splitZXTop(wp, y_split=0.0):
    return splitWithPlane(
        wp,
        cq.Plane.named(
            "ZX", 
            origin=(0,y_split,0)),
        keepTop = True)
    
def splitZXBottom(wp, y_split=0.0):
    return splitWithPlane(
        wp,
        cq.Plane.named(
            "ZX", 
            origin=(0,y_split,0)),
        keepBottom = True)
    
def splitYZTop(wp, x_split=0.0):
    return splitWithPlane(
        wp,
        cq.Plane.named(
            "YZ", 
            origin=(x_split,0,0)),
        keepTop = True)
    
def splitYZBottom(wp, x_split=0.0):
    return splitWithPlane(
        wp,
        cq.Plane.named(
            "YZ", 
            origin=(x_split,0,0)),
        keepBottom = True)

def createSketchFromDXFs(
    fnames_and_modes: Iterable[Iterable[str]], 
    tol: float=1e-3) -> cq.Sketch:
    """
    fnames_and_modes is a collection of string pairs. 
    First member of pair is mode (see cq.Sketch.Modes).
    Second member of pair a path to DXF file
    """

    sketch = cq.Sketch()
    for (mode, fname) in fnames_and_modes:
        sketch = sketch.importDXF(
            os.path.abspath(fname), 
            tol=1e-3, 
            mode=mode)
    return sketch.clean()

def _midvalue(vmin: float, vmax: float) -> float:
    return vmin + (vmax-vmin)*0.5

def centeredSketch(sketch: cq.Sketch):
    # there is no access to Sketch._faces compound
    # through public members so to avoid
    # duplicating the compund just to calculate
    # bounding box midpoint 
    xmin = sys.float_info.max
    xmax = sys.float_info.min
    ymin = sys.float_info.max
    ymax = sys.float_info.min
    zmin = sys.float_info.max
    zmax = sys.float_info.min

    for face in sketch:
        bbox = face.BoundingBox()
        
        xmin = min(xmin, bbox.xmin)
        ymin = min(ymin, bbox.ymin)
        zmin = min(zmin, bbox.zmin)

        xmax = max(xmax, bbox.xmax)
        ymax = max(ymax, bbox.ymax)
        zmax = max(zmax, bbox.zmax)
    
    if xmin > xmax:
        # mo faces processed - sketch is empty
        return sketch
    
    return sketch.moved(
        cq.Location(-cq.Vector(
            _midvalue(xmin, xmax),
            _midvalue(ymin, ymax),
            _midvalue(zmin, zmax))))


def scaledSketch(s: cq.Sketch, scale_factor: float):
    t_matrix = cq.Matrix(
        [[scale_factor, 0,            0, 0], 
         [0,            scale_factor, 0, 0], 
         [0,            0,            1, 0]]
    )

    return (
        cq.Sketch()
        .face(
            cq.Compound.makeCompound(
                [face.transformGeometry(t_matrix) for face in s])
        )
        .clean()
    )

def setSketchSize(
    s: cq.Sketch, 
    width: Optional[float] = None, 
    height: Optional[float] = None):

    if not ((width is not None and width > 0.0) ^ 
            (height is not None and height > 0.0)):
        raise ValueError(
            "One (and only one) of width or height "
            + "arguments should be given positive value")

    get_coord_minmax = (
        lambda bbox: (bbox.xmin, bbox.xmax)
        if width
        else lambda bbox: (bbox.ymin, bbox.ymax)
    )

    global_min = sys.float_info.max
    global_max = sys.float_info.min
    for face in s:
        face_coord_min, face_coord_max = get_coord_minmax(face.BoundingBox())
        global_min = min(global_min, face_coord_min)
        global_max = max(global_max, face_coord_max)
    if global_min > global_max:
        # sketch contains no faces
        return s

    scale_factor = width / (global_max - global_min)
    return scaledSketch(s, scale_factor)
