# Self-Moving Mouse hardware design files (board and case)

[[_TOC_]]

see also

- [documentation](https://gitlab.com/fedorkotov/self-moving-mouse.documentation)
- [firmware](https://gitlab.com/fedorkotov/self-moving-mouse.firmware) repository
- [auxiliary tools](https://gitlab.com/fedorkotov/self-moving-mouse.tools) repository

## Device photos

<img src="case/device-photo-assembled.jpg" height="250">
<img src="case/device-photo-case-opened.jpg" height="250">

## About this revision

This is the second revision of the device with custom board and 
3D printed case. The first revision was just a 
[DigiSpark board](http://digistump.com/products/1) potted
inside a small plastic box together with two LEDs and a button
on a piece of proto-board.

## Case

I didn't want to wreck the case with self-tappers 
by opening and closing it multiple times during 
development process. Also I hate latches.
That is why the case consists of two 3D printed parts 
connected together with one wide head 
[chicago screw](https://en.wikipedia.org/wiki/Sex_bolt) 
through a hole in PCB. 

Case model was created with 
[CadQuery](https://github.com/CadQuery/cadquery),
an open-source, script based, parametric modeling 
python library that uses Open Cascade CAD Kernel 
internally (the same one that 
[FreeCad](https://github.com/FreeCAD/FreeCAD) 
is based on).

<img src="case/model_build_sequence/33_exploded_device.svg" width="1000px"><br>
<img src="case/model_build_sequence/28_full_model_with_board.svg" width="800px">

See also [model build sequence illustrations](case/ModelBuildSequence.md)

### Case design files

- main case model file is `case/case_cadquery.py`
  other python files in `case` folder contain auxiliary 
  functions and constants
- "Mouse on a wheel" SVG logo taken from 
  [flyclipart.com](https://flyclipart.com/)
  and redrawn with polylines (arc and line segments) 
  by me in FreeCAD and exported to DXFs
  can be found in `case/logo` folder
- case part STL and STEP models
  - case halves without logo
    - `case/models/case-bottom-no-logo.stl`  
    - `case/models/case-bottom-no-logo.step`  
    - `case/models/case-top-no-logo.stl`  
    - `case/models/case-top-no-logo.step`  
  - case halves with logo. Can be more difficult
    to 3D print or non-manufacturable depending
    on printer and technology used
    - `case/models/case-bottom-with-logo.stl`  
    - `case/models/case-bottom-with-logo.step`  
    - `case/models/case-top-with-logo.stl`  
    - `case/models/case-top-with-logo.step`    
  - button pusher
    - `case/models/button-pusher.stl`
    - `case/models/button-pusher.step`
- board model imported from KiCAD
  - `models/self-moving-mouse-board-z-adjusted.step`
  - `models/self-moving-mouse-board-z-adjusted.stl`

## PCB

<img src="pcb-design/kicad-project/self-moving-mouse/self-moving-mouse-pcb-3drender.png" height="400px">

![Schematics](pcb-design/kicad-project/self-moving-mouse/self-moving-mouse-schematics.svg)

Schematics is basically a copy of 
[Digispark](http://digistump.com/products/1) with 
LEDs and a pushbutton added and 
voltage regulator removed.

ATTiny85 microcontroller is powered from USB port 
through Schottky diode that serves both as reverse
polarity protection and to lower Voltage a bit
to make level conversion from `5V` to `3.3V` 
for USB D+/D- lines easier.
Level converter consists of two `BZT52C3V6` 
`3.6V` Zener diodes. `1 kOhm` pullup diode 
is connected to `D-` so that PC recognizes
Self-Moving Mouse as a low speed USB device.

The board has 4 layers and is `15x35 mm` 
with `1.6 mm` thickness.
USB-A male connector is located on one of 15 mm sides,
pushbutton and status LEDs at the other.

Programmer is connected via `2.54 mm` pitch 
Dupont header near one of the long sides
or via a custom-made pogo-pin adapter.
(dupont header througholes are filled with solder 
and used as contact pads)

Programming adapter is just a piece of protoboard
with 6 pogo pins and a standard 6-pin AVR ISP connector
with point-to-point wiring.

<img src="pcb-design/programming-adapter1.jpg" height="200">
<img src="pcb-design/programming-adapter2.jpg" height="200">

### Board design files 

- KiCAD project is in `pcb-design/kicad-project/self-moving-mouse` folder
- USB A and pushbutton FreeCAD models
  that were used to create custom KiCAD footprints
  - `pcb-design/kicad-project/side-pushbutton-3dmodel.FCStd`
  - `pcb-design/kicad-project/USB-DS1097-3dmodel.FCStd`
- Gerbers of the last board revision are in
  `pcb-design/kicad-project/gerbers-rev2.2`
  folder. They were produced according to
  JLC PCB requirements as of 2021. 
  You may need recreate them with different settings 
  if you order your boards somewhere else.
- BJT status LED driver calculation is in `pcb-design/calculations/LedSwitchingBJTTransistorSelection.ipynb` notebook
- Selecting the correct Zeners for level 
  converter was not easy.
  -  An account of my struggles can be found 
  in [README.md](pcb-design/README.md) 
  inside `pcb-design` folder.
  - SPICE models can be found in `pcb-design/spice-simulations` 
  folder
  - `MMBZ5227B` Zener diode characteristic curve
    measurement results are in `pcb-design/measurements` folder


## Useful links

* USB signalling in a nutshell https://www.beyondlogic.org/usbnutshell/usb2.shtml
* https://electronics.stackexchange.com/questions/451909/what-is-the-point-of-these-components-on-the-schematic-digispark-attiny85
* https://electronics.stackexchange.com/questions/452414/which-way-round-should-d3-go

## Legal Info

Files in this repository are licensed under [CC-BY-NC-SA version 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license 
with the following exceptions

* The following files can be used at your discretion either under `CC-BY-NC-SA version 4.0` or under `MIT` license
  * python code inside `case` folder
  * python code inside `pcb-design/spice-simulations` folder
  * python code inside `pcb-design/measurements` folder
* contents of `case/logo` folder is licensed under [CC-BY-NC version 4.0](https://creativecommons.org/licenses/by-nc/4.0/). These vector images are based on `case/logo/logo.svg` taken from [flyclipart.com](https://flyclipart.com/)
* SPICE models in `pcb-design/spice-simulations/submodels` folder were found on the internets. For many of them I can't say for sure who is the original author. Use at you own risk.
