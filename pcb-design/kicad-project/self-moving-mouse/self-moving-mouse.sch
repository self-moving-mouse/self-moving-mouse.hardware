EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Self-Moving Mouse"
Date "2022-02-08"
Rev "rev2.2.3"
Comp "Fedor Kotov"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20SU U1
U 1 1 602988D9
P 3200 2200
F 0 "U1" H 3500 2800 50  0000 R CNN
F 1 "ATtiny85-20SU" H 3800 1600 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 3200 2200 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 3200 2200 50  0001 C CNN
	1    3200 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 6029CC11
P 3200 3000
F 0 "#PWR0101" H 3200 2750 50  0001 C CNN
F 1 "GND" H 3205 2827 50  0000 C CNN
F 2 "" H 3200 3000 50  0001 C CNN
F 3 "" H 3200 3000 50  0001 C CNN
	1    3200 3000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 6029D122
P 3200 1400
F 0 "#PWR0102" H 3200 1250 50  0001 C CNN
F 1 "VCC" H 3215 1573 50  0000 C CNN
F 2 "" H 3200 1400 50  0001 C CNN
F 3 "" H 3200 1400 50  0001 C CNN
	1    3200 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1400 3200 1600
Wire Wire Line
	3200 2800 3200 3000
Text GLabel 4050 2400 2    50   Input ~ 0
RST
Wire Wire Line
	4050 2400 3900 2400
Text GLabel 4050 2000 2    50   BiDi ~ 0
MISO
Text GLabel 4050 1900 2    50   BiDi ~ 0
MOSI
Wire Wire Line
	3800 1900 4050 1900
Wire Wire Line
	3800 2000 4050 2000
Wire Wire Line
	2050 6050 2050 6100
$Comp
L Device:D_Schottky D1
U 1 1 602ADF26
P 2800 4850
F 0 "D1" V 2754 4930 50  0000 L CNN
F 1 "D_Schottky" V 2845 4930 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323" H 2800 4850 50  0001 C CNN
F 3 "~" H 2800 4850 50  0001 C CNN
F 4 "PMEG4005AEA" V 2950 5150 50  0000 C CNN "PartNumber"
	1    2800 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 5450 2800 5000
Wire Wire Line
	2800 4550 2800 4600
$Comp
L Device:D_Zener D2
U 1 1 602B13C5
P 2700 6100
F 0 "D2" V 2654 6180 50  0000 L CNN
F 1 "3.6" V 2745 6180 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" H 2700 6100 50  0001 C CNN
F 3 "~" H 2700 6100 50  0001 C CNN
F 4 "BZT52C3V6" V 2850 6350 50  0000 C CNN "PartNumber"
	1    2700 6100
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D3
U 1 1 602B1FA0
P 3300 6100
F 0 "D3" V 3254 6180 50  0000 L CNN
F 1 "3.6" V 3345 6180 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" H 3300 6100 50  0001 C CNN
F 3 "~" H 3300 6100 50  0001 C CNN
F 4 "BZT52C3V6" V 3450 6350 50  0000 C CNN "PartNumber"
	1    3300 6100
	0    1    1    0   
$EndComp
Text GLabel 4550 5350 2    50   BiDi ~ 0
USBP
Text GLabel 4550 5750 2    50   BiDi ~ 0
USBM
$Comp
L Device:R R3
U 1 1 602B5216
P 4100 5350
F 0 "R3" V 3893 5350 50  0000 C CNN
F 1 "68" V 3984 5350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4030 5350 50  0001 C CNN
F 3 "~" H 4100 5350 50  0001 C CNN
	1    4100 5350
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 602B5879
P 4100 5750
F 0 "R4" V 4307 5750 50  0000 C CNN
F 1 "68" V 4216 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4030 5750 50  0001 C CNN
F 3 "~" H 4100 5750 50  0001 C CNN
	1    4100 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 5350 4550 5350
Wire Wire Line
	4250 5750 4550 5750
Wire Wire Line
	2450 5650 2700 5650
Wire Wire Line
	3100 5650 3100 5350
$Comp
L power:GND #PWR0105
U 1 1 602C1C99
P 2700 6500
F 0 "#PWR0105" H 2700 6250 50  0001 C CNN
F 1 "GND" H 2705 6327 50  0000 C CNN
F 2 "" H 2700 6500 50  0001 C CNN
F 3 "" H 2700 6500 50  0001 C CNN
	1    2700 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 6250 2700 6500
$Comp
L Device:R R2
U 1 1 602D8B04
P 3500 4850
F 0 "R2" H 3570 4896 50  0000 L CNN
F 1 "1.0k" H 3570 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 4850 50  0001 C CNN
F 3 "~" H 3500 4850 50  0001 C CNN
	1    3500 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 4550 3500 4700
Wire Wire Line
	3800 2100 4050 2100
Text GLabel 4050 2100 2    50   Input ~ 0
SCK
Text GLabel 4050 2200 2    50   BiDi ~ 0
USBP
Text GLabel 4050 2300 2    50   BiDi ~ 0
USBM
Wire Wire Line
	3800 2200 4050 2200
Wire Wire Line
	3800 2300 4050 2300
$Comp
L power:GND #PWR0109
U 1 1 602FBEE7
P 7750 3000
F 0 "#PWR0109" H 7750 2750 50  0001 C CNN
F 1 "GND" H 7755 2827 50  0000 C CNN
F 2 "" H 7750 3000 50  0001 C CNN
F 3 "" H 7750 3000 50  0001 C CNN
	1    7750 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 602FCFA1
P 7750 1700
F 0 "D5" V 7789 1582 50  0000 R CNN
F 1 "LED" V 7698 1582 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 7750 1700 50  0001 C CNN
F 3 "~" H 7750 1700 50  0001 C CNN
F 4 "Red" V 7600 1500 50  0000 C CNN "Color"
	1    7750 1700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 602FE89E
P 7750 2150
F 0 "R7" H 7820 2196 50  0000 L CNN
F 1 "430" H 7820 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7680 2150 50  0001 C CNN
F 3 "~" H 7750 2150 50  0001 C CNN
	1    7750 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1850 7750 2000
Wire Wire Line
	7750 2300 7750 2450
Wire Wire Line
	7750 2850 7750 3000
$Comp
L Device:R R6
U 1 1 60302489
P 7150 2650
F 0 "R6" V 6943 2650 50  0000 C CNN
F 1 "12k" V 7034 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7080 2650 50  0001 C CNN
F 3 "~" H 7150 2650 50  0001 C CNN
	1    7150 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 2650 7450 2650
Text GLabel 6800 2650 0    50   Input ~ 0
MOSI
Wire Wire Line
	6800 2650 7000 2650
$Comp
L power:VCC #PWR0110
U 1 1 60304CC1
P 7750 1400
F 0 "#PWR0110" H 7750 1250 50  0001 C CNN
F 1 "VCC" H 7765 1573 50  0000 C CNN
F 2 "" H 7750 1400 50  0001 C CNN
F 3 "" H 7750 1400 50  0001 C CNN
	1    7750 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1400 7750 1550
$Comp
L Device:R R8
U 1 1 60318160
P 5650 2000
F 0 "R8" H 5720 2046 50  0000 L CNN
F 1 "10k" H 5720 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5580 2000 50  0001 C CNN
F 3 "~" H 5650 2000 50  0001 C CNN
	1    5650 2000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 6031900B
P 5650 2600
F 0 "SW1" V 5604 2748 50  0000 L CNN
F 1 "SW_Push" V 5695 2748 50  0000 L CNN
F 2 "smm-footprints:KLS7_TS6335_TURTLE_SIDE_SWITCH" H 5650 2800 50  0001 C CNN
F 3 "~" H 5650 2800 50  0001 C CNN
	1    5650 2600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 6031ABDB
P 5650 3000
F 0 "#PWR0111" H 5650 2750 50  0001 C CNN
F 1 "GND" H 5655 2827 50  0000 C CNN
F 2 "" H 5650 3000 50  0001 C CNN
F 3 "" H 5650 3000 50  0001 C CNN
	1    5650 3000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 6031B20C
P 5650 1400
F 0 "#PWR0112" H 5650 1250 50  0001 C CNN
F 1 "VCC" H 5665 1573 50  0000 C CNN
F 2 "" H 5650 1400 50  0001 C CNN
F 3 "" H 5650 1400 50  0001 C CNN
	1    5650 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1400 5650 1850
Wire Wire Line
	5650 2800 5650 3000
Wire Wire Line
	5650 2400 5650 2300
Text GLabel 5000 2300 0    50   Input ~ 0
MISO
Wire Wire Line
	5000 2300 5650 2300
Connection ~ 5650 2300
Wire Wire Line
	5650 2300 5650 2150
Text Notes 6550 2050 0    50   ~ 0
Status LED is driven with \nan NPN transistor for it \nto not interfere with ISP
Text Notes 4750 2100 0    50   ~ 0
Button pullup resistor\nvalue is high enough\nfor it to not interfere\nwith ISP
Wire Notes Line
	4700 1150 4700 3700
Wire Notes Line
	4700 3700 6200 3700
Wire Notes Line
	6200 3700 6200 1150
Wire Notes Line
	6200 1150 4700 1150
Text Notes 4700 1100 0    118  ~ 0
Button
Wire Notes Line
	6500 1150 10650 1150
Wire Notes Line
	10650 1150 10650 3700
Wire Notes Line
	10650 3700 6500 3700
Wire Notes Line
	6500 3700 6500 1150
Text Notes 6500 1100 0    118  ~ 0
Status LED
$Comp
L Transistor_BJT:MMBT3904 Q1
U 1 1 602C5C9E
P 7650 2650
F 0 "Q1" H 7841 2696 50  0000 L CNN
F 1 "MMBT3904" H 7841 2605 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7850 2575 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 7650 2650 50  0001 L CNN
F 4 "https://www.mouser.com/datasheet/2/149/2N3904-82270.pdf" H 7650 2650 50  0001 C CNN "AltDatasheet"
	1    7650 2650
	1    0    0    -1  
$EndComp
Wire Notes Line
	4400 1150 4400 3700
Wire Notes Line
	4400 3700 2550 3700
Wire Notes Line
	2550 3700 2550 1150
Wire Notes Line
	2550 1150 4400 1150
Text Notes 2550 1100 0    118  ~ 0
MCU
Wire Notes Line
	2250 1150 2250 3700
Wire Notes Line
	2250 3700 750  3700
Wire Notes Line
	750  3700 750  1150
Wire Notes Line
	750  1150 2250 1150
Text Notes 750  1100 0    118  ~ 0
AVR ISP Header
Wire Notes Line
	750  4100 6200 4100
Wire Notes Line
	6200 4100 6200 7700
Wire Notes Line
	6200 7700 750  7700
Wire Notes Line
	750  7700 750  4100
Text Notes 750  4050 0    118  ~ 0
USB connection
$Comp
L Device:R R1
U 1 1 602EEF4B
P 2050 6250
F 0 "R1" H 2120 6296 50  0000 L CNN
F 1 "0" H 2120 6205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 6250 50  0001 C CNN
F 3 "~" H 2050 6250 50  0001 C CNN
	1    2050 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 6050 2350 6050
Wire Wire Line
	2350 6050 2350 6450
Wire Wire Line
	2200 6450 2200 6500
$Comp
L power:GND #PWR0117
U 1 1 602ACAA4
P 2200 6500
F 0 "#PWR0117" H 2200 6250 50  0001 C CNN
F 1 "GND" H 2205 6327 50  0000 C CNN
F 2 "" H 2200 6500 50  0001 C CNN
F 3 "" H 2200 6500 50  0001 C CNN
	1    2200 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6450 2050 6450
Wire Wire Line
	2050 6450 2050 6400
Connection ~ 2200 6450
Text Notes 900  6500 0    50   ~ 0
USB shield is connected \nto GND through 0 Ohm \nresistor so that it can be \neasily disconnected if this \nwill turn out to cause \ninterference susceptibility \nissues.
Wire Wire Line
	3300 6250 3300 6500
$Comp
L power:GND #PWR0104
U 1 1 602C149B
P 3300 6500
F 0 "#PWR0104" H 3300 6250 50  0001 C CNN
F 1 "GND" H 3305 6327 50  0000 C CNN
F 2 "" H 3300 6500 50  0001 C CNN
F 3 "" H 3300 6500 50  0001 C CNN
	1    3300 6500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 6037A43C
P 2800 4550
F 0 "#PWR0103" H 2800 4400 50  0001 C CNN
F 1 "VCC" H 2815 4723 50  0000 C CNN
F 2 "" H 2800 4550 50  0001 C CNN
F 3 "" H 2800 4550 50  0001 C CNN
	1    2800 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6450 2350 6450
$Comp
L smmsymbols:USB_A_Device J1
U 1 1 602EE8DD
P 2150 5650
F 0 "J1" H 2207 6117 50  0000 C CNN
F 1 "USB_A_Device" H 2207 6026 50  0000 C CNN
F 2 "smm-footprints:USB_A_CONNFLY_DS1097" H 2300 5600 50  0001 C CNN
F 3 "" H 2300 5600 50  0001 C CNN
	1    2150 5650
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 6034E7C2
P 3500 4550
F 0 "#PWR0106" H 3500 4400 50  0001 C CNN
F 1 "VCC" H 3515 4723 50  0000 C CNN
F 2 "" H 3500 4550 50  0001 C CNN
F 3 "" H 3500 4550 50  0001 C CNN
	1    3500 4550
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60352625
P 2800 4600
F 0 "#FLG0101" H 2800 4675 50  0001 C CNN
F 1 "PWR_FLAG" V 2800 4727 50  0000 L CNN
F 2 "" H 2800 4600 50  0001 C CNN
F 3 "~" H 2800 4600 50  0001 C CNN
	1    2800 4600
	0    -1   -1   0   
$EndComp
Connection ~ 2800 4600
Wire Wire Line
	2800 4600 2800 4700
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 60380C10
P 1100 2050
F 0 "J2" H 1018 1525 50  0000 C CNN
F 1 "Conn_01x06" H 1018 1616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 1100 2050 50  0001 C CNN
F 3 "~" H 1100 2050 50  0001 C CNN
	1    1100 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1300 2250 1500 2250
Wire Wire Line
	1500 2250 1500 2600
$Comp
L power:GND #PWR0107
U 1 1 60387B0D
P 1500 2600
F 0 "#PWR0107" H 1500 2350 50  0001 C CNN
F 1 "GND" H 1505 2427 50  0000 C CNN
F 2 "" H 1500 2600 50  0001 C CNN
F 3 "" H 1500 2600 50  0001 C CNN
	1    1500 2600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 60388E54
P 1500 1400
F 0 "#PWR0108" H 1500 1250 50  0001 C CNN
F 1 "VCC" H 1515 1573 50  0000 C CNN
F 2 "" H 1500 1400 50  0001 C CNN
F 3 "" H 1500 1400 50  0001 C CNN
	1    1500 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1400 1500 2150
Wire Wire Line
	1500 2150 1300 2150
Wire Wire Line
	1300 2050 1650 2050
Wire Wire Line
	1650 1950 1300 1950
Wire Wire Line
	1300 1850 1650 1850
Wire Wire Line
	1300 1750 1650 1750
Text GLabel 1650 1750 2    50   BiDi ~ 0
MOSI
Text GLabel 1650 1850 2    50   BiDi ~ 0
MISO
Text GLabel 1650 1950 2    50   Output ~ 0
SCK
Text GLabel 1650 2050 2    50   Output ~ 0
RST
$Comp
L Device:R R9
U 1 1 603F520E
P 3900 1650
F 0 "R9" H 3970 1696 50  0000 L CNN
F 1 "10k" H 3970 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3830 1650 50  0001 C CNN
F 3 "~" H 3900 1650 50  0001 C CNN
	1    3900 1650
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 603F634D
P 3900 1400
F 0 "#PWR01" H 3900 1250 50  0001 C CNN
F 1 "VCC" H 3915 1573 50  0000 C CNN
F 2 "" H 3900 1400 50  0001 C CNN
F 3 "" H 3900 1400 50  0001 C CNN
	1    3900 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1400 3900 1500
Wire Wire Line
	3900 1800 3900 2400
Connection ~ 3900 2400
Wire Wire Line
	3900 2400 3800 2400
Text Notes 8500 1800 0    50   ~ 0
Ib - base current\nR6 - base resistor value\nVbeQ1 - base-emitter voltage of Q1\nhfeminQ1 - minimum gain of Q1\nIl -load (LED) current (assumed 5 mA)\nVcesatQ1 - saturation collector-emitter voltage of Q1\nVfD5 - forward voltage of D5 LED (1.95 V for red)
Text Notes 8500 2250 0    50   ~ 0
(1)  Ib*R6 + VbeQ1 = V_CC\n(2)  Ib*hfeminQ1/5 = Il \n(3)  R7*Il + VfD5 + VcesatQ1 = V_CC\n\nFactor of 5 in (2) is to ensure Q1 saturation
Text Notes 8500 3100 0    50   ~ 0
R6 = (hfeminQ1 * (V_CC-VbeQ1)) / 5Il \n    = 70 * (5 - 0.75) / (5* 0.005 )\n    = 11900 Ohm \n\nR7 = (V_CC - VcesatQ1 - VfD5) / Il\n    = (5 - 1 - 1.95) / 0.005 \n    = 410 Ohm\n\nClosest standard values are \nR6=12k Ohm and R7 = 430 Ohm
Text Notes 8500 3600 0    50   ~ 0
Currents and dissipation with standard R6, R7 values\n\nIl = 4.8 mA\nIb = 0.35 mA\nQ1 dissipation = Il*VcesatQ1 + Ib*VbeQ1 = 5 mW
Text Notes 3900 7650 0    50   ~ 0
Using the right kind of Zeners is critical for the device \noperation.\nBy standard \nD+/D- high level is 2.8..3.6V\nD+/D- low level is 0..0.3V\nThis seems to be unattainable with just zeners. Either high \nlevel is too high or R2 can not pull D- to 2.8 without \nIO pin assistance or low level is too high.\n\nMMBZ5227B do not work (tested by me)\n\nBZT52C3V6 produce D- high of 3.8V (+0.2V higher\nthan max allowed). But physical\nchinese Digispark clone board I have uses them \nand works ok. That is why I have chosen them\nfor self-moving mouse.\n\nBZX84C3V6 seem to produce same\nlevels as BZT52C3V6\n\nOther projects also mention 1N5227B\nwere used successfully.
Wire Wire Line
	2450 5750 3300 5750
Wire Wire Line
	2700 5950 2700 5650
Connection ~ 2700 5650
Wire Wire Line
	2700 5650 3100 5650
Wire Wire Line
	3300 5950 3300 5750
Connection ~ 3300 5750
Wire Wire Line
	3300 5750 3500 5750
Wire Wire Line
	3100 5350 3950 5350
Wire Wire Line
	3500 5000 3500 5750
Wire Wire Line
	3500 5750 3950 5750
Text Notes 3400 3600 0    50   ~ 0
        !!!ATENTION!!!\nUSBP and USBM are\nswapped compared \nto the original Digispark \nschematics to make \nrouting easier. \nDon't forget to\nchange definitions in\nusbconfig.h
Text Notes 3900 4950 0    50   ~ 0
The original Digispark schematics specifies \nR2=1.5k. Comments in V-USB library source code\nalso say it should be 1.5k but the resistor on \na physical Digispark board I have measures 1k.\nAnd it also works fine in self-moving mouse.
Wire Wire Line
	6950 4500 7200 4500
Connection ~ 6950 4500
Wire Wire Line
	6950 4350 6950 4500
$Comp
L power:VCC #PWR0113
U 1 1 603742CA
P 6950 4350
F 0 "#PWR0113" H 6950 4200 50  0001 C CNN
F 1 "VCC" H 6965 4523 50  0000 C CNN
F 2 "" H 6950 4350 50  0001 C CNN
F 3 "" H 6950 4350 50  0001 C CNN
	1    6950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4500 6950 4500
Text Notes 9100 4050 2    118  ~ 0
PWR Indicator
Wire Notes Line
	7850 5500 7850 4100
Wire Notes Line
	9100 5500 7850 5500
Wire Notes Line
	9100 4100 9100 5500
Wire Notes Line
	7850 4100 9100 4100
Wire Wire Line
	8450 4350 8450 4450
Wire Wire Line
	8450 5150 8450 5250
Wire Wire Line
	8450 4750 8450 4850
$Comp
L power:GND #PWR0116
U 1 1 60352603
P 8450 5250
F 0 "#PWR0116" H 8450 5000 50  0001 C CNN
F 1 "GND" H 8455 5077 50  0000 C CNN
F 2 "" H 8450 5250 50  0001 C CNN
F 3 "" H 8450 5250 50  0001 C CNN
	1    8450 5250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0115
U 1 1 6034D7D5
P 8450 4350
F 0 "#PWR0115" H 8450 4200 50  0001 C CNN
F 1 "VCC" H 8465 4523 50  0000 C CNN
F 2 "" H 8450 4350 50  0001 C CNN
F 3 "" H 8450 4350 50  0001 C CNN
	1    8450 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 6034D7C9
P 8450 5000
F 0 "R5" H 8520 5046 50  0000 L CNN
F 1 "1k" H 8520 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8380 5000 50  0001 C CNN
F 3 "~" H 8450 5000 50  0001 C CNN
	1    8450 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 6034D66F
P 8450 4600
F 0 "D4" V 8489 4482 50  0000 R CNN
F 1 "LED" V 8398 4482 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 8450 4600 50  0001 C CNN
F 3 "~" H 8450 4600 50  0001 C CNN
F 4 "Green" V 8300 4350 50  0000 C CNN "Color"
	1    8450 4600
	0    -1   -1   0   
$EndComp
Text Notes 6500 4050 0    118  ~ 0
Decoupling
Wire Notes Line
	7550 4100 6500 4100
Wire Notes Line
	7550 5500 7550 4100
Wire Notes Line
	6500 5500 7550 5500
Wire Notes Line
	6500 4100 6500 5500
Wire Wire Line
	6950 5000 7200 5000
Connection ~ 6950 5000
Wire Wire Line
	6950 5150 6950 5000
$Comp
L power:GND #PWR0114
U 1 1 60325D25
P 6950 5150
F 0 "#PWR0114" H 6950 4900 50  0001 C CNN
F 1 "GND" H 6955 4977 50  0000 C CNN
F 2 "" H 6950 5150 50  0001 C CNN
F 3 "" H 6950 5150 50  0001 C CNN
	1    6950 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5000 7200 4900
Wire Wire Line
	6700 5000 6950 5000
Wire Wire Line
	6700 4900 6700 5000
Wire Wire Line
	7200 4500 7200 4600
Wire Wire Line
	6700 4600 6700 4500
$Comp
L Device:C C2
U 1 1 6031E8CB
P 7200 4750
F 0 "C2" H 7315 4796 50  0000 L CNN
F 1 "0.1u" H 7315 4705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7238 4600 50  0001 C CNN
F 3 "~" H 7200 4750 50  0001 C CNN
	1    7200 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 60319AE8
P 6700 4750
F 0 "C1" H 6815 4796 50  0000 L CNN
F 1 "4.7u" H 6815 4705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6738 4600 50  0001 C CNN
F 3 "~" H 6700 4750 50  0001 C CNN
	1    6700 4750
	1    0    0    -1  
$EndComp
Connection ~ 3500 5750
Wire Wire Line
	2800 5450 2450 5450
Text Notes 800  3600 0    50   ~ 0
1x6 pin header instead of \nthe standard 2x3 ISP header \nfor the device to fit inside \na case with USB-flash drive-like \ndimensions.\nOn production units it will have \nto be desoldered after programming \nor special adapter with pogo-pins\nshould be used for programming
Text Notes 900  5150 0    50   ~ 0
D1 is flipped compared to\nthe original Digispark schematic. \nVoltage drop on the diode\nhelps to lower D+/D- high\nlevels a bit (though they are\nstill about 0.2V higher than 3.6V \nallowed by USB standard).\nIt also reduces the possibility of\ndamage to the PC if the device\nis accidentally powered through \nJ2 when also connected to USB \n(doing so is NOT recommended).
Text Notes 2200 7650 0    50   ~ 0
BZT52C3V6 are packaged in \nSOD-123 case but D2 and D3 \nhave SOT-23 footprint.\nSOT-23 is selected so that \nany of popular small surface-mount \nzeners can be used.\nSOD-123 can be easily soldered \non SOT-23 footprint manually \nbut the reverse would be much harder.
$EndSCHEMATC
