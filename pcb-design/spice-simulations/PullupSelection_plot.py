#!/usr/bin/env python

import collections
import sys
import re
from pathlib import Path
import itertools
from math import atan2,degrees

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, LogLocator, NullFormatter

DATA_FILE_NAME = re.compile(
    r'^USB_DMinus_ACTIVE_(HIGH|LOW)_(\w+)_(\d+)$', 
    re.IGNORECASE)

if len(sys.argv)<2:
  print("ERROR: no output file name provided")
  sys.exit(1)

if len(sys.argv)<3:
  print("ERROR: no title provided")
  sys.exit(1)

if len(sys.argv)<4:
  print("ERROR: no data file names provided")
  sys.exit(1)


output_file_name = sys.argv[1]
plot_title = sys.argv[2]

data_files_list= collections.defaultdict(
    lambda: collections.defaultdict(
        lambda: collections.defaultdict()))
for data_file in sys.argv[3].split(';'):
    dat_file_name = Path(data_file).stem
    m = DATA_FILE_NAME.match(dat_file_name)
    if not m:
        print(f"ERROR: '{dat_file_name}' is not a valid data file name")
        sys.exit(2)

    zener_model = m.group(2)
    pullup_value = m.group(3)
    level = m.group(1) 
    
    data_files_list[zener_model][pullup_value][level] = data_file

for z, zpl in data_files_list.items():
    for p, pl in zpl.items():
        if not 'HIGH' in pl:
            print(f"ERROR: missing HIGH data file for zener {z} and Rpullup={p}")
            sys.exit(2)
        if not 'LOW' in pl:
            print(f"ERROR: missing LOW data file for zener {z} and Rpullup={p}")
            sys.exit(2)

def read_data_file(fpath):
    data = np.genfromtxt(fpath, skip_header=1)
    v_in = data[:,0]
    v_cc = data[:,1]
    v_pin = data[:,2]
    v_dminus = data[:,3]
    i_iopin = data[:,4]
    i_zener = data[:,5]
    i_supply = data[:,6]
    return (
        v_in,         
        v_dminus)


#https://stackoverflow.com/questions/16992038/inline-labels-in-matplotlib
def labelLine(line,x,label=None,align=True,**kwargs):

    ax = line.axes
    xdata = line.get_xdata()
    ydata = line.get_ydata()

    if (x < xdata[0]) or (x > xdata[-1]):
        print('x label location is outside data range!')
        return

    #Find corresponding y co-ordinate and angle of the line
    ip = 1
    for i in range(len(xdata)):
        if x < xdata[i]:
            ip = i
            break

    y = ydata[ip-1] + (ydata[ip]-ydata[ip-1])*(x-xdata[ip-1])/(xdata[ip]-xdata[ip-1])

    if not label:
        label = line.get_label()

    if align:
        #Compute the slope
        dx = xdata[ip] - xdata[ip-1]
        dy = ydata[ip] - ydata[ip-1]
        ang = degrees(atan2(dy,dx))

        #Transform to screen co-ordinates
        pt = np.array([x,y]).reshape((1,2))
        trans_angle = ax.transData.transform_angles(np.array((ang,)),pt)[0]

    else:
        trans_angle = 0

    #Set a bunch of keyword arguments
    if 'color' not in kwargs:
        kwargs['color'] = line.get_color()

    if ('horizontalalignment' not in kwargs) and ('ha' not in kwargs):
        kwargs['ha'] = 'center'

    if ('verticalalignment' not in kwargs) and ('va' not in kwargs):
        kwargs['va'] = 'center'

    if 'backgroundcolor' not in kwargs:
        kwargs['backgroundcolor'] = ax.get_facecolor()

    if 'clip_on' not in kwargs:
        kwargs['clip_on'] = True

    if 'zorder' not in kwargs:
        kwargs['zorder'] = 2.5

    ax.text(x,y,label,rotation=trans_angle,**kwargs)

#https://stackoverflow.com/questions/16992038/inline-labels-in-matplotlib
def labelLines(lines,align=True,xvals=None,**kwargs):

    ax = lines[0].axes
    labLines = []
    labels = []

    #Take only the lines which have labels other than the default ones
    for line in lines:
        label = line.get_label()
        if "_line" not in label:
            labLines.append(line)
            labels.append(label)

    if xvals is None:
        xmin,xmax = ax.get_xlim()
        xvals = np.linspace(xmin,xmax,len(labLines)+2)[1:-1]

    for line,x,label in zip(labLines,xvals,labels):
        labelLine(line,x,label,align,**kwargs)


fig, (ax1, ax2) = plt.subplots(1,2)
fig.set_size_inches(14, 7)

#ax1.axhline(
#  y=2.8, 
#  color='r', linestyle='dotted', linewidth=2, 
#  label='USB High min (2.8V and max 3.8V)')
#ax1.axhline(
#  y=3.8, 
#  color='r', linestyle='dotted', linewidth=2)


ax2.axhline(
  y=0.3, 
  color='r', linestyle='dotted', linewidth=2, 
  label='USB Low max = 0.3V')


for z, zpl in data_files_list.items():
    v_in_1500, v_dminus_1500 = read_data_file(zpl['1500']['HIGH'])
    for p, pl in zpl.items():
        v_in, v_dminus = read_data_file(pl['HIGH'])

        ax1.plot(
            v_in, v_dminus - v_dminus_1500,   
            label=f'R={p}')

        v_in, v_dminus = read_data_file(pl['LOW'])

        ax2.plot(
            v_in, v_dminus,   
            label=f'R={p}')


if plot_title:
  fig.suptitle(plot_title, fontsize=12)

ax1.minorticks_on()
ax1.grid(which='major', color='k', linestyle='-')
ax1.grid(which='minor', color='k', linestyle='dotted')
#ax1.legend()
ax1.set_ylabel('$\Delta D-_{high}$ [V] (difference with $D-$ at $R_{pullup}$ = 1500 Ohm)')
ax1.set_xlabel('USB $V+$ [V]')
labelLines(ax1.get_lines(),align=True,fontsize=10)

ax2.minorticks_on()
ax2.grid(which='major', color='k', linestyle='-')
ax2.grid(which='minor', color='k', linestyle='dotted')
ax2.set_ylabel('$D-_{low}$ [V]')
ax2.set_xlabel('USB $V+$ [V]')
labelLines(ax2.get_lines(),align=True,fontsize=10)
#ax2.legend()

fig.tight_layout(rect=[0, 0, 1, 0.97])

fig.savefig(output_file_name)            
