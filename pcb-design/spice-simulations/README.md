# Scripts and models

To run simulations and recalculate plots
in this folder you will need the following:

* `ngspice` (http://ngspice.sourceforge.net/)
* `python` 3 with `numpy`, `matplotlib` and `doit` packages installed. [doit](https://pydoit.org/) is a pythonic
`make` alternative. `dodo.py` is an analogue of Makefile

To run all the simulations run `doit`. 
It will execute all the tasks in `dodo.py`.

To remove all temporary files and results run `doit clean`.

File descriptions:
* `dodo.py` - an analogue of Makefile for [doit](https://pydoit.org/)
* SPICE subcircuit models
    * `submodels/Zener_*.sub` files are SPICE subcircuit models of zener diodes. I have found multiple BZX84C3V6 models on the Internet. 
    Can't say for sure which one is more accurate.
    * `submodels/Schottky_PMEG4005AEA.sub` is a SPICE subcirquit model of Schottky diode used for reverse voltage protection.
* SPICE Model templates
    * `ZenerIVCurves.cirtemplate` is a SPICE model template 
    that is used to plot IV curves for each of Zener `*.sub` model.
    `dodo.py` script substitutes zener file name and zener 
    subcirquit name into this file to create `*.cir` models
    inside `spice-cirquits` folder.
    * `USB_DPlus_HIGH.cirtemplate`, `USB_DMinus_ACTIVE_HIGH.cirtemplate` 
    are spice model templates that are used to determine 
    USB D+/D- high levels for selected zener `*.sub` models when
    actively driven by MCU pins. Make file substitutes zener 
    file name and zener subcirquit name into these files to 
    create `*.cir` models inside `spice-cirquits` folder.
    * `USB_DMinus_IDLE_HIGH.cirtemplate` is a SPICE model template
    that is used to determine USB D- high level when driven
    only by pullup resistor (MCU pin in high impedance state)
* Python scripts for result plotting    
    * `IVplot.py`
    * `USB_DMinusIdleHigh_plot.py` 
    * `PullupSelection_plot.py`
    * `USB_DPlusMinusActiveHigh_plot.py`
    * `USB_DPlusMinusActiveLow_plot.py` 
* LaTeX + CircuiTikZ circuit diagrams
    * `ZenerIVCurves-schematic.tex`
    * `USB_DMinus_ACTIVE_HIGH-schematic.tex`
    * `USB_DMinus_ACTIVE_LOW-schematic.tex`
    * `USB_DMinus_IDLE_HIGH-schematic.tex`
    * `USB_DPlus_HIGH-schematic.tex`
    