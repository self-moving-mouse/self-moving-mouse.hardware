#!/usr/bin/env python

import sys
import re

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, LogLocator, NullFormatter

if len(sys.argv)<2:
  print("ERROR: no output file name provided")
  sys.exit(1)

if len(sys.argv)<3:
  print("ERROR: no data file name provided")
  sys.exit(1)

if len(sys.argv)<4:
  print("ERROR: no output pin label provided")
  sys.exit(1)  

output_pin_label = sys.argv[3]
plot_title = sys.argv[4] if len(sys.argv)>=4 else None

output_file_name = sys.argv[1]
input_file_name = sys.argv[2]

data = np.genfromtxt(input_file_name, skip_header=1)

v_in = data[:,0]
v_cc = data[:,1]
v_pin_low = data[:,2]
v_dplusminus_low = data[:,3]
i_iopin = data[:,4]
i_zener = data[:,5]
i_supply = data[:,6]
  
fig, (ax1, ax2) = plt.subplots(2,1)
fig.set_size_inches(7, 14)

#ax1.axvline(
#  x=5, 
#  color='g', linestyle='dotted', linewidth=3, 
#  label='USB + Nominal Voltage (5V)')
ax1.axhline(
  y=0.3, 
  color='r', linestyle='dotted', linewidth=2, 
  label='USB Low max = 0.3V')

ax1.set_ylabel('Node voltage [V]')
ax1.set_xlabel('USB V+ [V]')

# --- Voltages ---

#ax1.plot(
#  v_in, v_cc, 
#  label='VCC')
ax1.plot(
  v_in, v_pin_low, 
  label='ATtiny pin low level')  
ax1.plot(
  v_in, v_dplusminus_low,   
  label=f'USB {output_pin_label} low level')

#ax2.axvline(
#  x=5, 
#  color='g', linestyle='dotted', linewidth=3, 
#  label='USB V+ Nominal Voltage (5V)')

ax1.minorticks_on()
ax1.grid(which='major', color='k', linestyle='-')
ax1.grid(which='minor', color='k', linestyle='dotted')
ax1.legend()

# --- Currents ---

ax2.set_ylabel('Current [mA]')
ax2.set_xlabel('USB V+ [V]')

ax2.plot(
  v_in, i_iopin*1000., 
  label='IO pin current')

ax2.plot(
  v_in, i_zener*1000., 
  label='Current through Zener diode')

ax2.plot(
  v_in, i_supply*1000., 
  label='Supply current')  

ax2.minorticks_on()
ax2.grid(which='major', color='k', linestyle='-')
ax2.grid(which='minor', color='k', linestyle='dotted')
ax2.legend()

if plot_title:
  fig.suptitle(plot_title, fontsize=12)

fig.tight_layout(rect=[0, 0, 1, 0.97])

fig.savefig(output_file_name)
