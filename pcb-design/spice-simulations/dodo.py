# python doit file
# (alternative to Make)
# https://pydoit.org

DOIT_CONFIG = {
    'default_tasks': [
        'iv_plot',
        'dplus_high_plot',        
        'dminus_active_high_plot',
        'dminus_idle_high_plot',
        'latex_schematics_png',
        #'dminus_active_high_spice',
        #'dminus_active_low_spice',
        'dminus_active_low_plot',
        'influence_of_rpulllup_on_dminus',
        #'iv_cleanup',
        #'dplus_high_cleanup',        
        #'dminus_active_high_cleanup',
        #'dminus_idle_high_cleanup',
        'latex_schematics_cleanup']}


import pathlib
from typing import Dict
from pathlib import Path
import re

from doit.task import clean_targets

SPICE_CMD = 'ngspice -b'
LATEXRUN = 'latexrun'
CONVERT = 'convert'

SUBMODELS_FOLDER = 'submodels'
SPICE_CIRQUITS_FOLDER = 'spice-cirquits'
SPICE_OUT_FOLDER = 'spice-output'
PLOTS_FOLDER = 'plots'


ZENER_MODEL_NAMES = [
    'NXP_BZX84C3V6',
    'DI_BZX84C3V6',
    'DI_BZX84C3V6S',
    'DI_BZX84C3V6T',
    'DI_BZX84C3V6TS',
    'DI_BZX84C3V6W',
    'DI_MMBZ5227B',
    'UNKNOWN_MMBZ5227B']

SELECTED_ZENER_MODEL_NAMES = [
    'DI_BZX84C3V6',
    'NXP_BZX84C3V6',
    'DI_MMBZ5227B',
    'UNKNOWN_MMBZ5227B']

PULLUP_VALUES = [   
    390, 
    430, 
    470, 
    510, 
    560, 
    620, 
    680, 
    750, 
    820, 
    910, 
    1000, 
    1100, 
    1200, 
    1300, 
    1500]    

NOMINAL_PULLUP = 1500


SCHOTTKY_SUB_NAME = 'Schottky_PMEG4005AEA.sub'

#--------------------------------------------------------------------
# -- Auxiliary functions
#--------------------------------------------------------------------

def substitute(
    f_name_in: str,
    f_name_out: str,
    substitutions: Dict[str,str]) -> None:
    """
    envsubst tool replacement

    inspired by https://stackoverflow.com/a/6117124
    """
    replacements = {
        f"${{{k}}}": v
        for k,v in substitutions.items()}
    regex_replacements = \
        re.compile(
            "|".join(
                map(re.escape, replacements.keys())))

    input = Path(f_name_in).read_text()
    output = regex_replacements.sub(
        lambda m: replacements[m.group(0)],
        input)
    Path(f_name_out).write_text(output)

def simple():
    print("ok")

def _get_cir_path(cir_fname):
    return str(Path(SPICE_CIRQUITS_FOLDER) / f"{cir_fname}.cir")

def _get_dat_path(dat_fname):
    return str(Path(SPICE_OUT_FOLDER) / f"{dat_fname}.dat")

def _get_plot_path(plot_fname):
    return str(Path(PLOTS_FOLDER) / f"{plot_fname}")    

def _get_subcirquit_path(model_name):
    return str(Path(SUBMODELS_FOLDER) / f"{model_name}")      

SCHOTTKY_SUB_PATH = _get_subcirquit_path(SCHOTTKY_SUB_NAME)

def _get_cir_task(    
    cirtemplate: str,    
    zener_name: str,
    dat_fpath: str,
    cir_fpath: str,    
    additional_params: Dict[str,str] = {},
    name: str = None):

    sub_fname= _get_subcirquit_path(f'Zener_{zener_name}.sub')
    return {
        'name': name if name else zener_name,
        'targets':[cir_fpath],
        'clean':[clean_targets, simple],
        'file_dep':[sub_fname, cirtemplate],
        'actions': [(
            substitute,
            [cirtemplate,
             cir_fpath,
             {'SUBCQTNAME':zener_name,
              'SUBCQTFILE':sub_fname,
              'DAT_PATH': dat_fpath} | additional_params])]}                

def _get_spice_task(
    zener_name: str,
    cir_fpath: str,
    dat_fpath: str,
    name: str = None):

    sub_fname= _get_subcirquit_path(f'Zener_{zener_name}.sub')
    return {
            'name': name if name else zener_name,
            'targets': [dat_fpath],
            'file_dep':[cir_fpath, SCHOTTKY_SUB_PATH, sub_fname],
            'clean':[clean_targets, simple],
            'actions': [f'{SPICE_CMD} {cir_fpath} || echo "WARN: ngspice non-zero exit status will be ignored"']
        }

def _remove_file_if_exists(fpath):
        path = pathlib.Path(fpath)
        if path.is_file() and path.exists():
            print(f"'{fpath}' will be removed")
            path.unlink()

#--------------------------------------------------------------------
# -- Zener diode model IV curves
#--------------------------------------------------------------------

def task_iv_curve_cir():
    for zener_name in ZENER_MODEL_NAMES:
        yield _get_cir_task(
            'ZenerIVCurves.cirtemplate',
            zener_name,            
            _get_dat_path(f'IV_{zener_name}'),
            _get_cir_path(f'IV_{zener_name}') )

def task_iv_curve_spice():
    for zener_name in ZENER_MODEL_NAMES:
        yield _get_spice_task(
            zener_name,
            _get_cir_path(f'IV_{zener_name}'),
            _get_dat_path(f'IV_{zener_name}'))

def task_iv_plot():
    PLOT = _get_plot_path('ZenerIVCurves.png')
    PLOT_SCRIPT = 'IVplot.py'
    DAT_FILES = [
        _get_dat_path(f'IV_{z}') 
        for z in ZENER_MODEL_NAMES]
    return {
        'targets': [PLOT],
        'file_dep':
            DAT_FILES+
            [PLOT_SCRIPT],
        'clean':[clean_targets, simple],
        'actions': [f'./{PLOT_SCRIPT} {PLOT} {" ".join(DAT_FILES)}']
    }

def task_iv_cleanup():
    def remove_cir_files():
        for zener_name in ZENER_MODEL_NAMES:
            _remove_file_if_exists(
                _get_cir_path(f'IV_{zener_name}')) 
        
    return {
        'task_dep': [ 
            'iv_curve_spice'],
        'actions': [remove_cir_files]
    }      

#--------------------------------------------------------------------
# -- USB D+ high levels
#--------------------------------------------------------------------
def task_dplus_high_cir():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        yield _get_cir_task(
            'USB_DPlus_HIGH.cirtemplate',
            zener_name,            
            _get_dat_path(f'USB_DPlus_HIGH_{zener_name}'),
            _get_cir_path(f'USB_DPlus_HIGH_{zener_name}'))

def task_dplus_high_spice():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        yield _get_spice_task(
            zener_name,
            _get_cir_path(f'USB_DPlus_HIGH_{zener_name}'),
            _get_dat_path(f'USB_DPlus_HIGH_{zener_name}'))

def task_dplus_high_plot():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        PLOT = _get_plot_path(f'USB_DPlus_HIGH_{zener_name}.png')
        PLOT_SCRIPT = 'USB_DPlusMinusActiveHigh_plot.py'
        DAT_FILE = _get_dat_path(f'USB_DPlus_HIGH_{zener_name}')
        yield {
            'name': zener_name,
            'targets': [PLOT],
            'file_dep':
                [DAT_FILE, PLOT_SCRIPT],
            'clean':[clean_targets, simple],
            'actions': [f'./{PLOT_SCRIPT} {PLOT} {DAT_FILE} "D+" "USB D+ high level for {zener_name} zener model"']
        }

def task_dplus_high_cleanup():
    def remove_cir_files():
        for zener_name in SELECTED_ZENER_MODEL_NAMES:
            _remove_file_if_exists(
                _get_cir_path(f'USB_DPlus_HIGH_{zener_name}')) 
        
    return {
        'task_dep': [ 
            'dplus_high_spice'],
        'actions': [remove_cir_files]
    }          

#--------------------------------------------------------------------
# -- USB D- high (when actively driven) levels
#--------------------------------------------------------------------
def task_dminus_active_high_cir():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        for pullup_value in PULLUP_VALUES:
            yield _get_cir_task(
                'USB_DMinus_ACTIVE_HIGH.cirtemplate',
                zener_name,            
                _get_dat_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{pullup_value}'),
                _get_cir_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{pullup_value}'),
                additional_params={
                    'PULLUP_VALUE': str(pullup_value)
                },
                name = f"{zener_name}_{pullup_value}")

def task_dminus_active_high_spice():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        for pullup_value in PULLUP_VALUES:
            yield _get_spice_task(
                zener_name,
                _get_cir_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{pullup_value}'),
                _get_dat_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{pullup_value}'),
                name = f"{zener_name}_{pullup_value}")

def task_dminus_active_high_plot():
    PLOT_SCRIPT = 'USB_DPlusMinusActiveHigh_plot.py'
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        PLOT = _get_plot_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{NOMINAL_PULLUP}.png')        
        DAT_FILE = _get_dat_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{NOMINAL_PULLUP}')
        yield {
            'name': f"{zener_name}_{NOMINAL_PULLUP}",
            'targets': [PLOT],
            'file_dep':
                [DAT_FILE, PLOT_SCRIPT],
            'clean':[clean_targets, simple],
            'actions': [f'./{PLOT_SCRIPT} {PLOT} {DAT_FILE} "D-" "USB D- high level (actively driven) for {zener_name} zener model"']
        }

def task_dminus_active_high_cleanup():
    def remove_cir_files():
        for zener_name in SELECTED_ZENER_MODEL_NAMES:
            for pullup_value in PULLUP_VALUES:
                _remove_file_if_exists(
                    _get_cir_path(f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{pullup_value}')) 
        
    return {
        'task_dep': [ 
            'dminus_active_high_spice'],
        'actions': [remove_cir_files]
    }    

#--------------------------------------------------------------------
# -- USB D- low (when actively driven) levels
#--------------------------------------------------------------------
def task_dminus_active_low_cir():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        for pullup_value in PULLUP_VALUES:
            yield _get_cir_task(
                'USB_DMinus_ACTIVE_LOW.cirtemplate',
                zener_name,            
                _get_dat_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{pullup_value}'),
                _get_cir_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{pullup_value}'),
                additional_params={
                    'PULLUP_VALUE': str(pullup_value)
                },
                name = f"{zener_name}_{pullup_value}")

def task_dminus_active_low_spice():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        for pullup_value in PULLUP_VALUES:
            yield _get_spice_task(
                zener_name,
                _get_cir_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{pullup_value}'),
                _get_dat_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{pullup_value}'),
                name = f"{zener_name}_{pullup_value}")

def task_dminus_active_low_plot():
    PLOT_SCRIPT = 'USB_DPlusMinusActiveLow_plot.py'
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        PLOT = _get_plot_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{NOMINAL_PULLUP}.png')        
        DAT_FILE = _get_dat_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{NOMINAL_PULLUP}')
        yield {
            'name': f"{zener_name}_{NOMINAL_PULLUP}",
            'targets': [PLOT],
            'file_dep':
                [DAT_FILE, PLOT_SCRIPT],
            'clean':[clean_targets, simple],
            'actions': [f'./{PLOT_SCRIPT} {PLOT} {DAT_FILE} "D-" "USB D- low level (actively driven) for {zener_name} zener model"']
        }

def task_dminus_active_low_cleanup():
    def remove_cir_files():
        for zener_name in SELECTED_ZENER_MODEL_NAMES:
            for pullup_value in PULLUP_VALUES:
                _remove_file_if_exists(
                    _get_cir_path(f'USB_DMinus_ACTIVE_LOW_{zener_name}_{pullup_value}')) 
        
    return {
        'task_dep': [ 
            'dminus_active_low_spice'],
        'actions': [remove_cir_files]
    } 


#--------------------------------------------------------------------
# -- USB D- high (when idle) levels
#--------------------------------------------------------------------
def task_dminus_idle_high_cir():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        yield _get_cir_task(
            'USB_DMinus_IDLE_HIGH.cirtemplate',
            zener_name,            
            _get_dat_path(f'USB_DMinus_IDLE_HIGH_{zener_name}'),
            _get_cir_path(f'USB_DMinus_IDLE_HIGH_{zener_name}'),
            additional_params={
                'PULLUP_VALUE': '1.5k'
            })

def task_dminus_idle_high_spice():
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        yield _get_spice_task(
            zener_name,
            _get_cir_path(f'USB_DMinus_IDLE_HIGH_{zener_name}'),
            _get_dat_path(f'USB_DMinus_IDLE_HIGH_{zener_name}'))

def task_dminus_idle_high_plot():
    PLOT_SCRIPT = 'USB_DMinusIdleHigh_plot.py'
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        PLOT = _get_plot_path(f'USB_DMinus_IDLE_HIGH_{zener_name}.png')        
        DAT_FILE = _get_dat_path(f'USB_DMinus_IDLE_HIGH_{zener_name}')
        yield {
            'name': zener_name,
            'targets': [PLOT],
            'file_dep':
                [DAT_FILE, PLOT_SCRIPT],
            'clean':[clean_targets, simple],
            'actions': [f'./{PLOT_SCRIPT} {PLOT} {DAT_FILE} "D-" "USB D- high level (idle) for {zener_name} zener model"']
        }    

def task_dminus_idle_high_cleanup():
    def remove_cir_files():
        for zener_name in SELECTED_ZENER_MODEL_NAMES:
            _remove_file_if_exists(
                _get_cir_path(f'USB_DMinus_IDLE_HIGH_{zener_name}')) 
        
    return {
        'task_dep': [ 
            'dminus_idle_high_plot'],
        'actions': [remove_cir_files]
    }          

#--------------------------------------------------------------------
# -- Influence of pullup resistor value on D- levels
#--------------------------------------------------------------------
def task_influence_of_rpulllup_on_dminus():
    PLOT_SCRIPT = 'PullupSelection_plot.py'
    
    for zener_name in SELECTED_ZENER_MODEL_NAMES:
        PLOT = _get_plot_path(f'USB_DMinus_Rpullup_Influence_{zener_name}.png')        
        DAT_FILES = [_get_dat_path(
                f'USB_DMinus_ACTIVE_LOW_{zener_name}_{pullup_value}')
            for pullup_value in PULLUP_VALUES] + \
            [_get_dat_path(
                f'USB_DMinus_ACTIVE_HIGH_{zener_name}_{pullup_value}')
            for pullup_value in PULLUP_VALUES]            

        DAT_FILES_FOR_CMDLINE = \
            ";".join(DAT_FILES)

        yield {
            'name': zener_name,
            'targets': [PLOT],
            'file_dep':
                DAT_FILES + [PLOT_SCRIPT],
            'clean':[clean_targets, simple],
            'actions': [ f"./{PLOT_SCRIPT} {PLOT} 'Influence of $R_{{pullup}}$ on D- levels' '{DAT_FILES_FOR_CMDLINE}'"]
        }    

#--------------------------------------------------------------------
# -- Latex schematics
#-------------------------------------------------------------------- 
def task_latex_schematics_pdf():
    for f in pathlib.Path().glob('*-schematic.tex'):
        yield {
            'name':f.stem,
            'file_dep':[str(f)],
            'targets': [f"{f.stem}.pdf"],
            'actions': [f"{LATEXRUN} {f}"]}

def task_latex_schematics_png():
    for f in pathlib.Path().glob('*-schematic.tex'):
        yield {
            'name':f.stem,
            'file_dep': [f"{f.stem}.pdf"],            
            'targets': [f"{f.stem}.png"],
            'actions': [
                f"{CONVERT} -density 130 {f.stem}.pdf -background white -flatten {f.stem}.png"]}

def task_latex_schematics_cleanup():
    def remove_schematic_pdf_files():
        for f in pathlib.Path().glob('*-schematic.tex'):
            _remove_file_if_exists(
                f"{f.stem}.pdf") 
        
    return {
        'task_dep': [ 
            'latex_schematics_png'],
        'actions': [remove_schematic_pdf_files]
    } 