#!/usr/bin/env python

import sys
import re
from pathlib import Path

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, LogLocator, NullFormatter

DATA_FILE_NAME = re.compile(r'^IV_(.*)$', re.IGNORECASE)

if len(sys.argv)<2:
  print("ERROR: no output file name provided")
  sys.exit(1)

if len(sys.argv)<3:
  print("ERROR: no data file names provided")
  sys.exit(1)

output_file_name = sys.argv[1]

data_index=[]
for data_file in sys.argv[2:]:
  dat_file_name = Path(data_file).stem
  m = DATA_FILE_NAME.match(dat_file_name)
  if not m:
    print(f"ERROR: '{dat_file_name}' is not a valid IV data file name")
    sys.exit(2)
  data_index.append([
    m.group(1),
    data_file])
  
fig, ax = plt.subplots()
fig.set_size_inches(7, 7)

ax.axvline(x=3.6, color='r', label='Vz = 3.6 V')
ax.axvline(x=2.8, color='r', linestyle='dotted', label='USB High min (2.8V and max 3.8V)')
ax.axvline(x=3.8, color='r', linestyle='dotted')

for label, fpath in data_index:
  data = np.genfromtxt(fpath)
  ax.plot(
    data[:,0],
    data[:,1]*1000.0,
    label=label)


ax.set_ylabel('I [mA]')
ax.set_xlabel('V [V]')
ymin, ymax = ax.get_ylim()
ax.set_ylim([1e-3,ymax])

ax.set_yscale('log')

ml = MultipleLocator(0.1)

y_minor = LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)

ax.minorticks_on()
ax.xaxis.set_minor_locator(ml)
ax.yaxis.set_minor_locator(y_minor)
ax.yaxis.set_minor_formatter(NullFormatter())

ax.grid(which='major', color='k', linestyle='-')
ax.grid(which='minor', color='k', linestyle='dotted')

ax.legend()

fig.tight_layout()

fig.savefig(output_file_name)
