USB D PLUS HIGH Level (when actively driven high by MCU pin) for ${SUBCQTNAME} Zener

.include submodels/Schottky_PMEG4005AEA.sub
.include ${SUBCQTFILE}

* USB + voltage source
V1 VIN 0 5

* protection diode
X1 VCC VIN PMEG4005AEA

* ATTiny IO pin model
* (See chapter 22.6 "pin driver strength" of datasheet)
* This simple 1 resistor IO pin model is valid up 
* to 20mA of source current
VMEAS1 VCC RIOPIN_1
RIOPIN RIOPIN_1 VPINHIGH 25

* series resistor
R3 VPINHIGH DMINUS 68

* pullup resistor
R2 DMINUS VCC ${PULLUP_VALUE} 

* Zener based USB D- level converter
VMEAS2 DMINUS X3Anode
X3 0 X3Anode ${SUBCQTNAME}

* Not sure about load resistance
RLOAD DMINUS 0 100k

.control
    dc V1 4.0 6.0 0.01
    print VCC, VPINHIGH, DMINUS, VMEAS1#branch, -V1#branch
    set wr_singlescale
    set wr_vecnames
    * ngspice bug workaround.
    * have to define supply_current so that 
    * -V1#branch expression will not be combined
    * into ingle expression with previous vector name below
    * (coma separator does not help with that)
    let supply_current = -V1#branch
    let io_pin_current = VMEAS1#branch
    let zener_current = VMEAS2#branch
    wrdata ${DAT_PATH} VCC, VPINHIGH, DMINUS, io_pin_current, zener_current, supply_current    
.endc
.end
