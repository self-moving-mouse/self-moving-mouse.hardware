# Choosing zener diodes and D- pull up resistor for level converter

## TL;DR

After a lot of trial and error, googling and SPICE modelling
I came to the conclusion that getting both low and high
D+/D- voltage levels within USB spec with simple Zener 
based level converter is impossible and publicly available
SPICE models of `3.6V` Zeners can not be trusted.

Of everything I have tried the most optimal combination
of components is
- `BZT52C3V6` Zener diodes
- `1.0 kOhm` `D-` pull-up resistor

With them high level exceeds max allowed value by `0.2V`
but this does not cause any problems.

[[_TOC_]]

## Files 

See 
* [spice-simulations/README.md](spice-simulations/README.md) 
* [measurements/README.md](measurements/README.md) 

for annotated list of models and scripts
that were used to get results in this section
and instructions on how to reproduce them

## Zener selection saga

Publicly available Digispark schematic does not include
specific 3.6V zener diode model used for level conversion
from ~5V VCC to `USB D+/D-` high level (3.3V nominal).

Initially I chose `BZX84C3V6` `3.6 V` zeners
(just because they were available from our
local electronic parts supplier with fast
delivery times). 
With them and `1.5k` `D-` pull-up I measured static 
`USB D+`/`USB D-` high levels around `4 V` 
which is considerably higher than maximum 
allowed value of `3.6 V`. 
By static I mean measured on the device with special 
firmware that keeps data pin high.
The device was powered from lab power supply 
set to 5V.
Elevated `USB D+`/`USB D-` levels were measured both
when power was connected through
`D1` Schottky protection diode and directly.

Then I created SPICE models (see sections
[Zener diode IV curves](#zener-diode-iv-curves-from-spice-models),
[Modelling USB D+ high level](#modelling-usb-d-high-level)
and [Modelling USB D- high level when driven by IO pin](#modelling-usb-d-high-level-when-driven-by-io-pin)) 
and they confirmed what I 
had measured. `BZX84C3V6` are not 'stiff' enough, 
meaning zener current does not rise fast 
enough with rising input voltage for effective regulation.

`MMBZ5227B` `3.6V` zeners that were also available from 
local electronic parts shop seemed to be Ok according to SPICE
simulations. This was confirmed by measurements when I
later installed them into device.

What SPICE models failed to predict was `~2.6 V` `USB D-`
high level when it is driven only by 
`1.5k` `R2` pull-up resistor and IO pin is in high impedance state.
With `MMBZ5227B`s installed this level 
(see [Modelling USB D- high level when driven only by pull-up](#modelling-usb-d-high-level-when-driven-only-by-pullup)).
According to low speed USB spec minimum high level is `2.8V`.

On the physical Digispark board I have `R2` is `1k`
but with `MMBZ5227B` this increases `USB D-` high
level only by about ~0.1V which is not enough.

Datasheet for `MMBZ522X` does not include I-V curve 
so I had to do measurements myself to figure out the cause 
of this discrepancy between SPICE and reality
(see [Measuring MMBZ5227B IV curve](#measuring-mmbz5227b-iv-curve)).
I discovered that IV curve produced by `MMBZ5227B` SPICE models
was more or less accurate only towards the higher end of
`2.8 .. 3.6 V` allowed voltage range of `USB D+/-` high level.
Near the lower end the current is much higher than 
predicted by SPICE models. So `R2` value has to be lowered
to at most `820 Ohm` to approach `2.8` from below and to 
`470` or `430` `Ohm` to raise it to `3.0 V` 
and have a margin of `0.2 V`.

With `430 Ohm` pull-up resistor I measured the following on real board
when powered directly from lab power supply set to `5 V` bypassing
`D1` protection diode
- `3.5V` `D-` high level when actively driven by IO pin which is Ok 
- `3.0V` `D-` high level when driven only by pull-up and IO in high 
   impedance state which is also Ok
- `0.8V` `D-` low level when actively driven by IO pin 
   which is **higher than maximum allowed low level of `0.3V`**
   and it does not change much when the device is powered through
   `D1`

Then I carefully analyzed google results for
"V-USB zenner" request and found out that 
other projects reported success only
with `BZT52C3V6` and `1N5227B`.
I examined more closely my physical Digispark board
and discovered that is most likely uses
`BZT52C3V6` (3 stripes on anode, W4 marking).
I desoldered zeners from the Digispark and 
transferred them to my board and got
the following results with `1k` `D-` pull-up
- `3.8V` `D-` high level when actively driven by IO pin
- `3.1V` `D-` high level when driven only by pull-up and IO in high 
   impedance state which is also Ok
- `0.38V` `D-` low level when actively driven by IO 

This is noticeably out of spec but works. 
The device is detected by host and as
low speed USB and V-USB works just fine and stable.
Prolonged use does not fry USB host's electronics
despite elevated voltage levels.


## SPICE modelling of USB D+/D- high levels

I found quite a few slightly different SPICE models
of `BZX84C3V6` and `MMBZ5227B` `3.6 V` Zeners:
- [Zener_DI_BZX84C3V6.sub](spice-simulations/submodels/Zener_DI_BZX84C3V6.sub)
- [Zener_DI_BZX84C3V6S.sub](spice-simulations/submodels/Zener_DI_BZX84C3V6S.sub)
- [Zener_DI_BZX84C3V6T.sub](spice-simulations/submodels/Zener_DI_BZX84C3V6T.sub)
- [Zener_DI_BZX84C3V6TS.sub](spice-simulations/submodels/Zener_DI_BZX84C3V6TS.sub)
- [Zener_DI_BZX84C3V6W.sub](spice-simulations/submodels/Zener_DI_BZX84C3V6W.sub)
- [Zener_NXP_BZX84C3V6.sub](spice-simulations/submodels/Zener_NXP_BZX84C3V6.sub)
- [Zener_DI_MMBZ5227B.sub](spice-simulations/submodels/Zener_DI_MMBZ5227B.sub)
- [Zener_UNKNOWN_MMBZ5227B.sub](spice-simulations/submodels/Zener_UNKNOWN_MMBZ5227B.sub)

After [comparing their I-V curves](#zener-diode-iv-curves-from-spice-models) 
I selected the following ones for further evaluation:
- Zener_DI_BZX84C3V6.sub
- Zener_NXP_BZX84C3V6.sub
- Zener_DI_MMBZ5227B.sub
- Zener_UNKNOWN_MMBZ5227B.sub

### Zener diode IV curves (from SPICE models)

SPICE model for modelling Zener IV curve:

![ZenerIVCurvesSPICEModel](spice-simulations/ZenerIVCurves-schematic.png)

Most `BZX84C3V6` models produce identical I-V curves.
`Zener_NXP_BZX84C3V6.sub`'s curve is different but
not different enough for `USB D-/D+` high levels to
become lower than maximum allowed `3.8 V` (see next sections).

`MMBZ5227` models are quite different. Measurements
showed that both of them are more or less correct 
around `3.6 ... 3.8 V` but give current values
much lower than tey really are near lower end of 
allowed range for `USB D+/-` high range/

![ZenerIVCurves](spice-simulations/plots/ZenerIVCurves.png)

### Modelling USB D+ high level

SPICE model of `USB D+` when driven by IO pin:

<img src="spice-simulations/USB_DPlus_HIGH-schematic.png" width="800
px">

See [USB_DPlus_HIGH.cirtemplate](spice-simulations/USB_DPlus_HIGH.cirtemplate)

- `R_IOPIN` value was taken from chapter 
22.6 "pin driver strength" of ATTiny datasheet. 
This simple 1 resistor IO pin model is valid 
up to 20mA of source current
- `R_LOAD` was selected arbitrarily as `100k` to represent 
load from PC's USB host electronics
- `D1` is Schottky protection diode (see [Schottky_PMEG4005AEA.sub](spice-simulations/submodels/Schottky_PMEG4005AEA.sub))
- `D2` is zener diode under test
- `VCC` is swept from 4 to 6 V to get an idea of
   what the model will do if VCC differs from nominal.

Both `BZX84C3V6` models give `USB D+` high 
level higher than maximum allowed `3.8 V`.

Both `MMBZ5227B` models give `USB D+` value 
well within allowed range.


#### USB D+ high level for Diodes Incorporated BZX84C3V6

<img src="spice-simulations/plots/USB_DPlus_HIGH_DI_BZX84C3V6.png" width="500
px">

#### USB D+ high level for NXP BZX84C3V6 zener model

<img src="spice-simulations/plots/USB_DPlus_HIGH_NXP_BZX84C3V6.png" width="500
px">

#### USB D+ high level for DI_MMBZ5227B zener model

<img src="spice-simulations/plots/USB_DPlus_HIGH_DI_MMBZ5227B.png" width="500
px">

#### USB D+ high level for UNKNOWN_MMBZ5227B zener model

<img src="spice-simulations/plots/USB_DPlus_HIGH_UNKNOWN_MMBZ5227B.png" width="500 px">


### Modelling USB D- high level when driven by IO pin

SPICE model of `USB D-` high level when driven by IO pin:

<img src="spice-simulations/USB_DMinus_ACTIVE_HIGH-schematic.png" width="700 px">


- `R_IOPIN` value was taken from chapter 
22.6 "pin driver strength" of ATTiny datasheet. 
This simple 1 resistor IO pin model is valid 
up to `20mA` of source current
- `R_LOAD` was selected arbitrarily as `100k` to represent 
load from PC's USB host electronics
- `R2` is `1.5 kOhm` pull-up resistor
- `D1` is Schotky protection diode (see [Schottky_PMEG4005AEA.sub](spice-simulations/submodels/Schottky_PMEG4005AEA.sub))
- `D2` is zener diode under test
- `VCC` is sweeped from 4 to 6 V to get an Idea of
   what the model will do if VCC differs from nominal.

Both `BZX84C3V6` models give `USB D-` high level higher than 
maximum allowed `3.8 V`.

Both `MMBZ5227B` models give `USB D-` value well within 
allowed range.   

#### USB D- high level when actively driven by MCU for Diodes Incorporated BZX84C3V6

<img src="spice-simulations/plots/USB_DMinus_ACTIVE_HIGH_DI_BZX84C3V6_1500.png" width="500 px">

#### USB D- high level when actively driven by MCU for NXP BZX84C3V6

<img src="spice-simulations/plots/USB_DMinus_ACTIVE_HIGH_NXP_BZX84C3V6_1500.png" width="500 px">

#### USB D- high level when actively driven by MCU for DI_MMBZ5227B

<img src="spice-simulations/plots/USB_DMinus_ACTIVE_HIGH_DI_MMBZ5227B_1500.png" width="500 px">

#### USB D- high level when actively driven by MCU for UNKNOWN_MMBZ5227B

<img src="spice-simulations/plots/USB_DMinus_ACTIVE_HIGH_UNKNOWN_MMBZ5227B_1500.png" width="500 px">


### Modelling USB D- high level when driven only by pull-up

SPICE model of `USB D-` high level when driven by IO pin:

<img src="spice-simulations/USB_DMinus_IDLE_HIGH-schematic.png" width="700 px">

- `R_LOAD` was selected arbitrarily as `100k` to represent 
load from PC's USB host electronics
- `R2` is `1.5 kOhm` pull-up resistor
- `D1` is Schotky protection diode (see [Schottky_PMEG4005AEA.sub](spice-simulations/Schottky_PMEG4005AEA.sub))
- `D2` is zener diode under test
- `VCC` is sweeped from 4 to 6 V to get an Idea of
   what the model will do if VCC differs from nominal.

All models show `USB D-` level above 2.8 V but this
turned out to be incorrect (see [Measuring MMBZ5227B IV curve](#measuring-mmbz5227b-iv-curve)). 
And `R2` value had to be lowered.

#### USB D- high level when idle for Diodes Incorporated BZX84C3V6

<img src="spice-simulations/plots/USB_DMinus_IDLE_HIGH_DI_BZX84C3V6.png" width="500 px">

#### USB D- high level when idle for NXP BZX84C3V6

<img src="spice-simulations/plots/USB_DMinus_IDLE_HIGH_NXP_BZX84C3V6.png" width="500 px">

#### USB D- high level when idle for DI_MMBZ5227B

<img src="spice-simulations/plots/USB_DMinus_IDLE_HIGH_DI_MMBZ5227B.png" width="500 px">

#### USB D- high level when idle for UNKNOWN_MMBZ5227B

<img src="spice-simulations/plots/USB_DMinus_IDLE_HIGH_UNKNOWN_MMBZ5227B.png" width="500 px">

## Measuring MMBZ5227B IV curve

All Zener SPICE models listed above turned out ot be
incorrect at low voltage values. Which lead to
unacceptably overestimated D- high level when driven
only by pull-up resistor. 

To discover this I had to measure characteristic curve of one
of the Zeners (see [README.md](measurements/README.md) in `measurements` folder)

To do that I went through a range of standard 
resistor values imitating pull-up `R2` and measured
a voltage drop over the resistor and over zener
(see schematic below), from that I calculated
current and plotted characteristic curve points
with error bars (accuracy taken from multimeter specs)

<img src="measurements/MMBZ5227B-IV-measurement-schematic.png" width="300 px">

<img src="measurements/20211211_Voltages.png" width="800 px">

<img src="measurements/20211211_IV.png" width="600 px">
