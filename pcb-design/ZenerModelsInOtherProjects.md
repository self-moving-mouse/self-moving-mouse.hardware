# Zener models in other projects using V-USB


Very few other projects name exact Zener models used.
General recommendations usually given are to use
3.6V Zeners with max power of 500 mW. 

Obviously it is not the max power per se that is critical
in this application but exact static IV characteristic curve 
of the device and internal capacitance.

Some sources state that zeners with
less than 500 mW max power would be better,
some say that exactly 500 mW is needed and zeners with
max power less than 500 mW do not work.

Some hobbyist say that the same zeners can work on
breadboard but not on PCB (or vice versa).
In one case scheme works when scope probe is
attached to D- but does not work without it and
a large (500k) resistor to ground imitating
influence of the scope probe fixed the problem
permanently.

Some concrete examples of zeners that do and do not work
for other people.

* `1N5227B` (throughole zeners) work on PCB for USnooBie project
* `1N5227B-TPCT-ND 500MW 3.6V DO35` (throughole zeners) work on breadboard but not on PCB
* `BZT52C3V6S-FDICT-ND` surface mount zener does not work on PCB 
in some projects but works for another
* `1N5227 (3.6 V, 500 mW)`
* `BZT52C3V6` works on PCB

## Sources

http://vusb.wikidot.com/hardware

> We recommend 3.6 V low power types, those 
> that look like 1N4148 (usually 500 mW or less). 
> Low power types are required because they have 
> less capacitance and thus cause less distortion 
> on the data lines. And 3.6 V is better than 3.3 V 
> because 3.3 V diodes yield only ca. 2.7 V in 
> conjunction with an 1.5 kΩ (or more exactly 10 kΩ) 
> pull-up resistor. With 3.3 V diodes, the device may 
> not be detected reliably.


https://codeandlife.com/2012/02/22/v-usb-with-attiny45-attiny85-without-a-crystal/

> The most common issues are probably too long wires, 
> too heavy (1W and above) zeners that don’t 
> "turn on" due to small amount of current, 
> and the USB connector should also be checked 
> for cold solder joints.
>
> [..]
>
> OK. It tooks me 10 hours to make this work. 
> Here, I was just able to get 3.6 V, 1 Watt zener diodes. 
> I’ve never been able to make it work. 
> Then I found 3.2 V, 1/4 Watt zener diodes. 
> Didn’t work. After many many attemps, I’ve begun to use 
> a scope to have an idea of what was going on… 
> and it worked! It was due to the scope impedance.
> So, I’ve added a 500 k resistance between ground 
> and D-. And it works!!


http://eleccelerator.com/usnoobie/assembly.php

> These should be 1N5227B 3.6V Zener diodes. 
> There have been reports that certain Zener 
> diodes will not work. 200mW Zener diodes may not
> work but 500mW Zener diodes will 

https://web.archive.org/web/20141020045140/http://forums.obdev.at/viewtopic.php?f=8&t=4677

> BZT52C3V6S-FDICT-ND DIODE ZENER 3.6V 200MW SOD-323

Does not work

> 1N5227B-TPCT-ND DIODE ZENER 500MW 3.6V DO35 

Does work (but only on breadboard - not on PCB)

https://electrikhelp.com/en/electrical-equipment/highfrequency-oscilloscope-from-a-computer-with-your-own-hands-how-to-make-an-oscilloscope-from-a-computer/

Uses BZX84C3V6 with 2.2k pullup

https://forums.obdev.at/viewtopic06d5.html?f=8&t=9365&p=28357

> 3.6V 0.5W zener diodes

https://github.com/obdev/v-usb/tree/2588689f2a8044cc2826cef2729f326a2eb593cd/circuits

>  This circuit enforces lower voltage levels 
> on D+ and D- with zener diodes.
> The zener diodes MUST be low power/low current 
> types to ensure that the 1k5 pull-up resistor on 
> D- generates a voltage of well above 2.5 V (but below 3.6 V). 
  
https://d.i10o.ca/projects/vusb-dev-board/

> The diodes I've got are the 1N5227 type (3.6 V, 500 mW).
> The other resistor is a 1.5 kΩ pull-up resistor,

https://www.fischl.de/usbasp/

> 3V6 zener	ZF 3,6

https://oshwlab.com/wagiminator/attiny85-usb-display

Uses BZT52C3V6 with 1.5k pullup

(!!! have w4 markings - same as digispark board I have)

