#!/usr/bin/env python

import sys

import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib.ticker import MultipleLocator, LogLocator, NullFormatter

if len(sys.argv)<2:
  print("ERROR: no output file prefix provided")
  sys.exit(1)

if len(sys.argv)<3:
  print("ERROR: no resistor measurements file provided")
  sys.exit(1)

if len(sys.argv)<3:
  print("ERROR: no voltage measurements file provided")
  sys.exit(1)  


OUTPUT_FILE_PREFIX = sys.argv[1]
RESISTOR_VALUES_CSV = sys.argv[2]
VOLTAGE_VALUES_CSV = sys.argv[3]

def read_csv(f_path):
    return pd.read_csv(
        f_path, 
        comment='#',
        skipinitialspace=True)

def get_half_error(
    accuracy_percent,
    range,
    accuracy_digits,
    resolution):

    return accuracy_percent*0.01*range + \
        accuracy_digits*resolution

def resistance_postprocessing(r_data):
    result = pd.DataFrame()
    result['Rnominal'] = r_data['Rnominal']
    result['RMeasuredMean'] = \
        r_data[['Rmeasured1','Rmeasured2','Rmeasured3']]\
        .mean(axis=1)
    result['RHalfError'] = \
        get_half_error(
            r_data.MulimeterAccuracyPercent,
            r_data.MultimeterRange,
            r_data.MultimeterAccuracyDigits,
            r_data.MultimeterResolution)
    return result

def voltage_postprocessing(v_data):
    result = pd.DataFrame()
    result['Rnominal'] = v_data['Rnominal']
    result['VHalfError'] = \
        get_half_error(
            v_data.MulimeterAccuracyPercent,
            v_data.MultimeterRange,
            v_data.MultimeterAccuracyDigits,
            v_data.MultimeterResolution)
    result['DeltaVRMean'] = \
        v_data[['DeltaVR1','DeltaVR2','DeltaVR3']]\
        .mean(axis=1)
    result['DeltaVDMean'] = \
        v_data[['DeltaVD1','DeltaVD2','DeltaVD3']]\
        .mean(axis=1)

    return result

    
def get_current(rv_data):
    current = rv_data.DeltaVRMean/rv_data.RMeasuredMean
    current_err_plus = \
        (rv_data.DeltaVRMean+rv_data.VHalfError)/ \
        (rv_data.RMeasuredMean-rv_data.RHalfError) - \
        current
    current_err_minus = \
        ((rv_data.DeltaVRMean-rv_data.VHalfError)/ \
        (rv_data.RMeasuredMean+rv_data.RHalfError) - \
        current).abs()
    
    return current, current_err_plus, current_err_minus


def get_power(
    voltage, 
    voltage_err, 
    current, 
    current_err_plus, 
    current_err_minus):

    power = voltage * current
    power_err_plus = \
        (voltage + voltage_err) * \
        (current + current_err_plus) - \
        power  
    power_err_minus = \
        ((voltage - voltage_err) * \
        (current - current_err_minus) - \
        power).abs()

    return power, power_err_plus, power_err_minus

def calculate_current_and_power(
    r_data,
    v_data):

    rvip_data = \
        pd.merge(
            r_data,
            v_data,
            on=["Rnominal", "Rnominal"])

    current, current_err_plus, current_err_minus = \
        get_current(rvip_data)
    rvip_data['Current'] = current
    rvip_data['CurrentErrorPlus'] = current_err_plus
    rvip_data['CurrentErrorMinus'] = current_err_minus

    power_r, power_r_err_plus, power_r_err_minus = \
        get_power(
            rvip_data.DeltaVRMean, 
            rvip_data.VHalfError, 
            rvip_data.Current, 
            rvip_data.CurrentErrorPlus, 
            rvip_data.CurrentErrorMinus)
    rvip_data['PowerR'] = power_r
    rvip_data['PowerRErrorPlus'] = power_r_err_plus
    rvip_data['PowerRErrorMinus'] = power_r_err_minus

    power_d, power_d_err_plus, power_d_err_minus = \
        get_power(
            rvip_data.DeltaVDMean, 
            rvip_data.VHalfError, 
            rvip_data.Current, 
            rvip_data.CurrentErrorPlus, 
            rvip_data.CurrentErrorMinus)
    rvip_data['PowerD'] = power_d
    rvip_data['PowerDErrorPlus'] = power_d_err_plus
    rvip_data['PowerDErrorMinus'] = power_d_err_minus

    return rvip_data

def _setup_ticks_x_axis_r_nominal_log(ax, r_nominal):
    ax.set_xticks(r_nominal)
    ax.minorticks_on()
    ax.grid(which='major', color='k', linestyle='-')
    ax.grid(which='minor', axis='y', color='k', linestyle='dotted')
    ax.set_xticklabels(r_nominal, rotation=60, ha='right')
    ax.tick_params(axis='x', which='minor', bottom=False)

    ax.set_xlabel('$R_{1,nominal}$ [Ohm], log scale')

    for item in (ax.get_xticklabels() + ax.get_yticklabels()):    
        item.set_fontsize(10)

def _setup_axis_label_font(ax):
    # https://stackoverflow.com/a/14971193
    for item in ([ax.xaxis.label, ax.yaxis.label]):
        item.set_fontsize(18)

def _get_matplotlib_asymmetric_error(
    error_minus,
    error_plus):
    return np.stack([
        error_minus.to_numpy(), 
        error_plus.to_numpy()])

def plot_resistance(
    rvip_data,
    output_path):

    fig, ax1 = plt.subplots()
    fig.set_size_inches(15, 7)

    ax1.errorbar(
        rvip_data.Rnominal,
        rvip_data.RMeasuredMean-rvip_data.Rnominal,
        yerr=rvip_data.RHalfError,
        elinewidth=5,
        fmt='.',
        color='k',
        ecolor='gray',
        label='$\Delta R = R_{1,measured}-R_{1,nominal}$')

    ax1.plot(
        rvip_data.Rnominal,
        rvip_data.Rnominal*0.05,
        color='k', 
        linestyle='dashed',
        linewidth=2,
        label='5% deviation from $R_{1,nominal}$')
    ax1.plot(
        rvip_data.Rnominal,
        -rvip_data.Rnominal*0.05,
        color='k', 
        linestyle='dashed', 
        linewidth=2)

    ax1.plot(
        rvip_data.Rnominal,
        rvip_data.Rnominal*0.01,
        color='k', 
        linestyle='dotted',
        linewidth=2,
        label='1% deviation from $R_{1,nominal}$')
    ax1.plot(
        rvip_data.Rnominal,
        -rvip_data.Rnominal*0.01,
        color='k',
        linestyle='dotted',
        linewidth=2)

    ax1.set_xscale('log')
    ax1.set_xlim([95,1600])
    
    ax1.set_ylabel('$\Delta R$ [Ohm]')

    _setup_ticks_x_axis_r_nominal_log(
        ax1,
        rvip_data.Rnominal)
        
    # https://stackoverflow.com/a/43439132
    ax1.legend(
        bbox_to_anchor=(0.5,-0.15), 
        loc="upper center", 
        ncol=2, 
        fontsize=15)

    _setup_axis_label_font(ax1)

    fig.savefig(output_path, dpi=600, bbox_inches='tight') 

def plot_voltages(
    rvip_data,
    output_path):

    fig, (ax2, ax1) = plt.subplots(2,1)
    fig.set_size_inches(15, 14)

    ax1.errorbar(
        rvip_data.RMeasuredMean,
        rvip_data.DeltaVRMean + rvip_data.DeltaVDMean,
        xerr=rvip_data.RHalfError,
        yerr=rvip_data.VHalfError*2.,
        color='k',
        ecolor='gray',
        fmt='.',
        elinewidth=5,
        label='$\Delta V_R + \Delta V_R$')

    ax1.axhline(
        5.0,
        color='k', 
        linestyle='dashed',
        linewidth = 3,
        label='$V_{CC,nominal} = 5.0$ [V]')

    ax1.set_xscale('log')
    ax1.set_xlim([90,1600])
    
    ax1.set_ylabel('$V$ [V]')

    _setup_ticks_x_axis_r_nominal_log(
        ax1,
        rvip_data.Rnominal)
    
    # https://stackoverflow.com/a/43439132
    ax1.legend(
        bbox_to_anchor=(0.5,-0.15), 
        loc="upper center", 
        ncol=2, 
        fontsize=15)

    _setup_axis_label_font(ax1)

    ax2.errorbar(
        rvip_data.RMeasuredMean,
        rvip_data.DeltaVRMean,
        xerr=rvip_data.RHalfError,
        yerr=rvip_data.VHalfError,
        color='k',
        fmt='.',
        ecolor='r',
        elinewidth=5,
        label='$\Delta V_R$')
    ax2.errorbar(
        rvip_data.RMeasuredMean,
        rvip_data.DeltaVDMean,
        xerr=rvip_data.RHalfError,
        yerr=rvip_data.VHalfError,
        color='k',
        fmt='.',
        ecolor='orange',
        elinewidth=5,
        label='$\Delta V_D$')

    ax2.set_xscale('log')
    ax2.set_xlim([90,1600])

    ax2.set_ylabel('$V$ [V]')

    _setup_ticks_x_axis_r_nominal_log(
        ax2,
        rvip_data.Rnominal)
    
    ax2.legend(
        bbox_to_anchor=(0.5,-0.15), 
        loc="upper center", 
        ncol=2, 
        fontsize=15)

    _setup_axis_label_font(ax2)

    plt.tight_layout()

    fig.savefig(output_path, dpi=600, bbox_inches='tight') 

def plot_current(
    rvip_data,
    output_path):

    fig, (ax3) = plt.subplots()
    fig.set_size_inches(15, 7)

    current_err = \
        _get_matplotlib_asymmetric_error(
            rvip_data.CurrentErrorMinus,
            rvip_data.CurrentErrorPlus)

    ax3.errorbar(
        rvip_data.RMeasuredMean,
        rvip_data.Current*1000.,
        xerr=rvip_data.RHalfError,
        yerr=current_err*1000.,
        ecolor='r',
        color='k',
        fmt='.',
        elinewidth=5,
        label='$I =\Delta V_D / R_{1,measured}$')

    ax3.set_xscale('log')
    ax3.set_xlim([90,1600])

    _setup_ticks_x_axis_r_nominal_log(
        ax3,
        rvip_data.Rnominal)

    ax3.set_ylabel('$I$ [mA]')
    
    ax3.legend(
        bbox_to_anchor=(0.5,-0.15), 
        loc="upper center", 
        ncol=2, 
        fontsize=15)

    _setup_axis_label_font(ax3)

    plt.tight_layout()

    fig.savefig(output_path, dpi=600, bbox_inches='tight') 

def plot_power(
    rvip_data,
    output_path):

    fig, (ax3) = plt.subplots()
    fig.set_size_inches(15, 7)

    r_power_err = _get_matplotlib_asymmetric_error(
        rvip_data.PowerRErrorMinus,
        rvip_data.PowerRErrorPlus)

    d_power_err = _get_matplotlib_asymmetric_error(
        rvip_data.PowerDErrorMinus,
        rvip_data.PowerDErrorPlus)

    ax3.errorbar(
        rvip_data.RMeasuredMean,
        rvip_data.PowerR*1000.,
        xerr=rvip_data.RHalfError,
        yerr=r_power_err*1000.,
        ecolor='r',
        color='k',
        fmt='.',
        elinewidth=5,
        label='$P_R = I \cdot \Delta V_R$')

    ax3.errorbar(
        rvip_data.RMeasuredMean,
        rvip_data.PowerD*1000.,
        xerr=rvip_data.RHalfError,
        yerr=d_power_err*1000.,
        ecolor='orange',
        color='k',
        fmt='.',
        elinewidth=5,
        label='$P_D = I \cdot \Delta V_D$')

    ax3.set_xscale('log')
    ax3.set_xlim([90,1600])

    ax3.set_ylabel('$P$ [mW]')
    _setup_ticks_x_axis_r_nominal_log(
        ax3,
        rvip_data.Rnominal)

    ax3.set_xlabel('$R_{1,measured}$ [Ohm], log scale')
        
    ax3.legend(
        bbox_to_anchor=(0.5,-0.15), 
        loc="upper center", 
        ncol=2, 
        fontsize=15)

    _setup_axis_label_font(ax3)

    plt.tight_layout()

    fig.savefig(output_path, dpi=600, bbox_inches='tight') 

def plot_iv(
    rvip_data,
    spice_iv_curves,
    output_path):

    fig, ax = plt.subplots()
    fig.set_size_inches(10, 10)

    ax.axvline(
        x=3.6, 
        color='gray', 
        linewidth = 3,
        label='Vz = 3.6 V')
    ax.axvline(
        x=2.8, 
        color='gray', 
        linestyle='dotted', 
        linewidth = 3,
        label='USB High min (2.8V and max 3.8V)')
    ax.axvline(
        x=3.8, 
        color='gray', 
        linewidth = 3,
        linestyle='dotted')

    current_err = \
        _get_matplotlib_asymmetric_error(
            rvip_data.CurrentErrorMinus,
            rvip_data.CurrentErrorPlus)

    ax.errorbar(
        rvip_data.DeltaVDMean,
        rvip_data.Current*1000.0,
        xerr=rvip_data.VHalfError,
        yerr=current_err*1000.0,
        ecolor='r',
        color='k',
        fmt='.',
        elinewidth=5,
        label='MMBZ5227B measured')

    for name, (voltage_spice, current_spice) in spice_iv_curves:
        ax.plot(
            voltage_spice,
            current_spice*1000.0,
            linewidth = 3,
            label=name)    

    ax.set_xlim([2.6,3.9])
    ax.set_ylim([0.5,50])
    ax.set_yscale('log')

    ax.set_ylabel('$I$ [mA]')
    ax.set_xlabel('$V$ [V]')
    ax.minorticks_on()
    ax.grid(which='major', color='k', linestyle='-')
    ax.grid(which='minor', color='k', linestyle='dotted')

    # https://stackoverflow.com/a/14971193
    for item in ([ax.xaxis.label, ax.yaxis.label]):
        item.set_fontsize(18)
    for item in (ax.get_xticklabels() + ax.get_yticklabels()):    
        item.set_fontsize(12)

    ax.legend(
        bbox_to_anchor=(0.5,-0.10), 
        loc="upper center", 
        ncol=1, 
        fontsize=15)

    plt.tight_layout()

    fig.savefig(output_path, dpi=600, bbox_inches='tight') 


r_data = resistance_postprocessing(
            read_csv(RESISTOR_VALUES_CSV))

v_data = voltage_postprocessing(
            read_csv(VOLTAGE_VALUES_CSV))            

rvip_data = calculate_current_and_power(
    r_data,
    v_data)

rvip_data.to_csv(
    OUTPUT_FILE_PREFIX+'_postprocessed.csv',
    float_format = '%.5f')

plot_resistance(
    rvip_data,
    OUTPUT_FILE_PREFIX+'_DeltaR.png')

plot_voltages(
    rvip_data,
    OUTPUT_FILE_PREFIX+'_Voltages.png')

plot_current(
    rvip_data,
    OUTPUT_FILE_PREFIX+'_Current.png')

plot_power(
    rvip_data,
    OUTPUT_FILE_PREFIX+'_Power.png')

def _read_spice_iv(f_path):
    iv_spice = np.genfromtxt(f_path)
    voltage_spice = iv_spice[:,0]
    current_spice = iv_spice[:,1]

    return voltage_spice, current_spice

spice_iv_curves = \
    [('DI_MMBZ5227B',
    _read_spice_iv('../spice-simulations/IV_DI_MMBZ5227B.dat')), 
    ('UNKNOWN_DI_MMBZ5227B',
    _read_spice_iv('../spice-simulations/IV_UNKNOWN_MMBZ5227B.dat')),
    ('DI_BZX84C3V6W',
    _read_spice_iv('../spice-simulations/IV_DI_BZX84C3V6W.dat'))]

plot_iv(
    rvip_data,
    spice_iv_curves,
    OUTPUT_FILE_PREFIX+'_IV.png')
 





