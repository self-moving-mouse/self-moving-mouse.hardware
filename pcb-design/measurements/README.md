# Measuring MMBZ5227B Zener diode characteristic curve

[[_TOC_]]

To recreate results in this section
you will need 
* python with `pandas`, `numpy`, `matplotlib` installed
* GNU `make` utility

To clean all intermediate files and results
run `make clean`. To recreate all plots
and postprocessing results run `make`

## Files list

* raw measurements data
  * `20211205-r-measured.csv` contains resistor 
    values measured with my RM111 multimeter 
    together with accuracy data from 
    meter's manual. Each resistor was
    measured 3 times for error
    averaging
  * voltage measurements (see schematic below).
    `D1` is `MMBZ5227B` each of resistors in 
    `20211205-r-measured.csv` was connected as `R1`.
    Each measurement was also repeated 3 times 
    * `20211208-delta-vr-delta-vd-measured.csv`
      first measurements session 
    * `20211211-delta-vr-delta-vd-measured.csv`
      second measurements session. 
      Second session included more resistor values.
* `iv-measurements-processing.py` postprocessing
  script that was used to calculate and plot
  for each of voltage measurement sessions
  * `*_DeltaR.png` - measured resistor values 
    deviation from nominal. It does not really matter
    for this experiment but was interesting to see.
  * `*_Current.png` - calculated current through zener 
  * `*_Voltages.png` - measured
    voltages plot
  * `*_Power.png` - power dissipated by resistor
    and zener
  * `*_IV.png` - characteristic calculated
    from measurements compared to characteristic
    curves produced by zener SPICE models 
* `MMBZ5227B-IV-measurement-schematic.tex` - 
  source file for LaTeX + CircuiTikZ
  schematic diagram 


## Schematic of measurement setup

<img src="MMBZ5227B-IV-measurement-schematic.png" width="300 px">  

## Measured resistor values deviation from nominal

<img src="20211211_DeltaR.png" width="600 px"> 

## Measured voltages

<img src="20211211_Voltages.png" width="600 px"> 

## Calculated current through Zener

<img src="20211211_Current.png" width="600 px"> 

## Calculated power dissipation for resistor and Zener

<img src="20211211_Power.png" width="600 px"> 

## Measures zener characteristic curve compared to characteristics produced by SPICE models

<img src="20211211_IV.png" width="600 px"> 